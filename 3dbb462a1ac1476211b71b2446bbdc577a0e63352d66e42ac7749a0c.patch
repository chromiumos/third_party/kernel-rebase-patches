From 9ee9a2e7784b127c57e815edf6034496eb632ab3 Mon Sep 17 00:00:00 2001
From: Daniele Ceraolo Spurio <daniele.ceraolospurio@intel.com>
Date: Wed, 28 Apr 2021 10:08:54 -0700
Subject: [PATCH] CHROMIUM: drm/i915/pxp: Implement ioctl actions to manage a
 user session

This patch allows a user to reserve and manage a PXP session. Creating
a session requires a 3-way handshake between i915, userspace and CSME/GSC
firmware, which follows these steps:

1 - user reserves a session ID with i915.
2 - user starts the session with CSME/GSC.
3 - user notifies i915 that the session has been started.

Destroyng a session requires a submission to the HW, which is performed
by i915 upon receiving the ioctl call from the user.

All sessions are terminated and the relevand IDs released when a
termination interrupt is received.

v2: check against drm_file when setting session in play (Alan)

Signed-off-by: Daniele Ceraolo Spurio <daniele.ceraolospurio@intel.com>
Reviewed by: Alan Previn <alan.previn.teres.alexis@intel.com>

BUG=b:191508777
TEST=tast run <IP> video.PlayDRM*

Signed-off-by: Juston Li <juston.li@intel.com>
Change-Id: I36b8515187ae8cb63a2674f4c14ac718ea2b4ab8
Reviewed-on: https://chromium-review.googlesource.com/c/chromiumos/third_party/kernel/+/3203915
Tested-by: Jeffrey Kardatzke <jkardatzke@google.com>
Reviewed-by: Jeffrey Kardatzke <jkardatzke@google.com>
Commit-Queue: Jeffrey Kardatzke <jkardatzke@google.com>
---
 drivers/gpu/drm/i915/i915_drv.c              |   3 +
 drivers/gpu/drm/i915/pxp/intel_pxp.c         | 116 ++++++++-
 drivers/gpu/drm/i915/pxp/intel_pxp.h         |   8 +
 drivers/gpu/drm/i915/pxp/intel_pxp_cmd.c     |  11 +-
 drivers/gpu/drm/i915/pxp/intel_pxp_cmd.h     |   8 +-
 drivers/gpu/drm/i915/pxp/intel_pxp_pm.c      |   2 +-
 drivers/gpu/drm/i915/pxp/intel_pxp_session.c | 241 ++++++++++++++++++-
 drivers/gpu/drm/i915/pxp/intel_pxp_session.h |  18 ++
 drivers/gpu/drm/i915/pxp/intel_pxp_types.h   |  34 ++-
 9 files changed, 407 insertions(+), 34 deletions(-)

diff --git a/drivers/gpu/drm/i915/i915_drv.c b/drivers/gpu/drm/i915/i915_drv.c
--- a/drivers/gpu/drm/i915/i915_drv.c
+++ b/drivers/gpu/drm/i915/i915_drv.c
@@ -67,6 +67,7 @@
 #include "gt/intel_gt_pm.h"
 #include "gt/intel_rc6.h"
 
+#include "pxp/intel_pxp.h"
 #include "pxp/intel_pxp_pm.h"
 
 #include "i915_debugfs.h"
@@ -1784,6 +1785,8 @@ static const struct drm_ioctl_desc i915_ioctls[] = {
 	DRM_IOCTL_DEF_DRV(I915_QUERY, i915_query_ioctl, DRM_RENDER_ALLOW),
 	DRM_IOCTL_DEF_DRV(I915_GEM_VM_CREATE, i915_gem_vm_create_ioctl, DRM_RENDER_ALLOW),
 	DRM_IOCTL_DEF_DRV(I915_GEM_VM_DESTROY, i915_gem_vm_destroy_ioctl, DRM_RENDER_ALLOW),
+
+	DOWNSTREAM_DRM_IOCTL_DEF_DRV(I915_PXP_OPS, i915_pxp_ops_ioctl, DRM_RENDER_ALLOW),
 };
 
 static const struct drm_driver driver = {
diff --git a/drivers/gpu/drm/i915/pxp/intel_pxp.c b/drivers/gpu/drm/i915/pxp/intel_pxp.c
--- a/drivers/gpu/drm/i915/pxp/intel_pxp.c
+++ b/drivers/gpu/drm/i915/pxp/intel_pxp.c
@@ -2,8 +2,10 @@
 /*
  * Copyright(c) 2020 Intel Corporation.
  */
+#include <linux/kernel.h>
 #include <linux/workqueue.h>
 #include "intel_pxp.h"
+#include "intel_pxp_cmd.h"
 #include "intel_pxp_irq.h"
 #include "intel_pxp_session.h"
 #include "intel_pxp_tee.h"
@@ -45,7 +47,7 @@ struct intel_gt *pxp_to_gt(const struct intel_pxp *pxp)
 
 bool intel_pxp_is_active(const struct intel_pxp *pxp)
 {
-	return pxp->arb_is_valid;
+	return pxp->arb_session.is_valid;
 }
 
 /* KCR register definitions */
@@ -108,8 +110,6 @@ void intel_pxp_init(struct intel_pxp *pxp)
 	if (!HAS_PXP(gt->i915))
 		return;
 
-	mutex_init(&pxp->tee_mutex);
-
 	/*
 	 * we'll use the completion to check if there is a termination pending,
 	 * so we start it as completed and we reinit it when a termination
@@ -121,19 +121,25 @@ void intel_pxp_init(struct intel_pxp *pxp)
 	mutex_init(&pxp->arb_mutex);
 	INIT_WORK(&pxp->session_work, intel_pxp_session_work);
 
+	mutex_init(&pxp->tee_mutex);
+	mutex_init(&pxp->session_mutex);
+
 	ret = create_vcs_context(pxp);
 	if (ret)
 		return;
 
+	intel_pxp_init_arb_session(pxp);
+
 	ret = intel_pxp_tee_component_init(pxp);
 	if (ret)
-		goto out_context;
+		goto out_session;
 
 	drm_info(&gt->i915->drm, "Protected Xe Path (PXP) protected content support initialized\n");
 
 	return;
 
-out_context:
+out_session:
+	intel_pxp_fini_arb_session(pxp);
 	destroy_vcs_context(pxp);
 }
 
@@ -142,16 +148,18 @@ void intel_pxp_fini(struct intel_pxp *pxp)
 	if (!intel_pxp_is_enabled(pxp))
 		return;
 
-	pxp->arb_is_valid = false;
+	pxp->arb_session.is_valid = false;
 
 	intel_pxp_tee_component_fini(pxp);
 
+	intel_pxp_fini_arb_session(pxp);
+
 	destroy_vcs_context(pxp);
 }
 
 void intel_pxp_mark_termination_in_progress(struct intel_pxp *pxp)
 {
-	pxp->arb_is_valid = false;
+	pxp->arb_session.is_valid = false;
 	reinit_completion(&pxp->termination);
 }
 
@@ -183,7 +191,7 @@ int intel_pxp_start(struct intel_pxp *pxp)
 
 	mutex_lock(&pxp->arb_mutex);
 
-	if (pxp->arb_is_valid)
+	if (pxp->arb_session.is_valid)
 		goto unlock;
 
 	pxp_queue_termination(pxp);
@@ -197,7 +205,7 @@ int intel_pxp_start(struct intel_pxp *pxp)
 	/* make sure the compiler doesn't optimize the double access */
 	barrier();
 
-	if (!pxp->arb_is_valid)
+	if (!pxp->arb_session.is_valid)
 		ret = -EIO;
 
 unlock:
@@ -244,3 +252,93 @@ int intel_pxp_key_check(struct intel_pxp *pxp,
 
 	return 0;
 }
+
+static int pxp_set_session_status(struct intel_pxp *pxp,
+				  struct downstream_drm_i915_pxp_ops *pxp_ops,
+				  struct drm_file *drmfile)
+{
+	struct downstream_drm_i915_pxp_set_session_status_params params;
+	struct downstream_drm_i915_pxp_set_session_status_params __user *uparams =
+		u64_to_user_ptr(pxp_ops->params);
+	int ret = 0;
+
+	if (copy_from_user(&params, uparams, sizeof(params)) != 0)
+		return -EFAULT;
+
+	switch (params.req_session_state) {
+	case DOWNSTREAM_DRM_I915_PXP_REQ_SESSION_ID_INIT:
+		ret = intel_pxp_sm_ioctl_reserve_session(pxp, drmfile,
+							 params.session_mode,
+							 &params.pxp_tag);
+		break;
+	case DOWNSTREAM_DRM_I915_PXP_REQ_SESSION_IN_PLAY:
+		ret = intel_pxp_sm_ioctl_mark_session_in_play(pxp, drmfile,
+							      params.pxp_tag);
+		break;
+	case DOWNSTREAM_DRM_I915_PXP_REQ_SESSION_TERMINATE:
+		ret = intel_pxp_sm_ioctl_terminate_session(pxp, drmfile,
+							   params.pxp_tag);
+		break;
+	default:
+		ret = -EINVAL;
+	}
+
+	if (ret >= 0) {
+		pxp_ops->status = ret;
+
+		if (copy_to_user(uparams, &params, sizeof(params)))
+			ret = -EFAULT;
+		else
+			ret = 0;
+	}
+
+	return ret;
+}
+
+int i915_pxp_ops_ioctl(struct drm_device *dev, void *data, struct drm_file *drmfile)
+{
+	int ret = 0;
+	struct downstream_drm_i915_pxp_ops *pxp_ops = data;
+	struct drm_i915_private *i915 = to_i915(dev);
+	struct intel_pxp *pxp = &i915->gt.pxp;
+	intel_wakeref_t wakeref;
+
+	if (!intel_pxp_is_enabled(pxp))
+		return -ENODEV;
+
+	wakeref = intel_runtime_pm_get_if_in_use(&i915->runtime_pm);
+	if (!wakeref) {
+		drm_dbg(&i915->drm, "pxp ioctl blocked due to state in suspend\n");
+		pxp_ops->status = DOWNSTREAM_DRM_I915_PXP_OP_STATUS_SESSION_NOT_AVAILABLE;
+		return 0;
+	}
+
+	if (!intel_pxp_is_active(pxp)) {
+		ret = intel_pxp_start(pxp);
+		if (ret)
+			goto out_pm;
+	}
+
+	mutex_lock(&pxp->session_mutex);
+
+	if (pxp->hw_state_invalidated) {
+		drm_dbg(&i915->drm, "pxp ioctl retry required due to state attacked\n");
+		pxp_ops->status = DOWNSTREAM_DRM_I915_PXP_OP_STATUS_RETRY_REQUIRED;
+		goto out_unlock;
+	}
+
+	switch (pxp_ops->action) {
+	case DOWNSTREAM_DRM_I915_PXP_ACTION_SET_SESSION_STATUS:
+		ret = pxp_set_session_status(pxp, pxp_ops, drmfile);
+		break;
+	default:
+		ret = -EINVAL;
+	}
+
+out_unlock:
+	mutex_unlock(&pxp->session_mutex);
+out_pm:
+	intel_runtime_pm_put(&i915->runtime_pm, wakeref);
+
+	return ret;
+}
diff --git a/drivers/gpu/drm/i915/pxp/intel_pxp.h b/drivers/gpu/drm/i915/pxp/intel_pxp.h
--- a/drivers/gpu/drm/i915/pxp/intel_pxp.h
+++ b/drivers/gpu/drm/i915/pxp/intel_pxp.h
@@ -6,6 +6,7 @@
 #ifndef __INTEL_PXP_H__
 #define __INTEL_PXP_H__
 
+#include <drm/drm_file.h>
 #include "intel_pxp_types.h"
 
 struct drm_i915_gem_object;
@@ -32,6 +33,8 @@ int intel_pxp_start(struct intel_pxp *pxp);
 int intel_pxp_key_check(struct intel_pxp *pxp,
 			struct drm_i915_gem_object *obj,
 			bool assign);
+
+int i915_pxp_ops_ioctl(struct drm_device *dev, void *data, struct drm_file *drmfile);
 #else
 static inline void intel_pxp_init(struct intel_pxp *pxp)
 {
@@ -57,6 +60,11 @@ static inline int intel_pxp_key_check(struct intel_pxp *pxp,
 {
 	return -ENODEV;
 }
+
+static inline int i915_pxp_ops_ioctl(struct drm_device *dev, void *data, struct drm_file *drmfile)
+{
+	return -ENODEV;
+}
 #endif
 
 #endif /* __INTEL_PXP_H__ */
diff --git a/drivers/gpu/drm/i915/pxp/intel_pxp_cmd.c b/drivers/gpu/drm/i915/pxp/intel_pxp_cmd.c
--- a/drivers/gpu/drm/i915/pxp/intel_pxp_cmd.c
+++ b/drivers/gpu/drm/i915/pxp/intel_pxp_cmd.c
@@ -92,12 +92,13 @@ static void pxp_request_commit(struct i915_request *rq)
 	mutex_unlock(&tl->mutex);
 }
 
-int intel_pxp_terminate_session(struct intel_pxp *pxp, u32 id)
+int intel_pxp_terminate_sessions(struct intel_pxp *pxp, long mask)
 {
 	struct i915_request *rq;
 	struct intel_context *ce = pxp->ce;
 	u32 *cs;
-	int err = 0;
+	int idx;
+	int err;
 
 	if (!intel_pxp_is_enabled(pxp))
 		return 0;
@@ -112,13 +113,14 @@ int intel_pxp_terminate_session(struct intel_pxp *pxp, u32 id)
 			goto out_rq;
 	}
 
-	cs = intel_ring_begin(rq, SESSION_TERMINATION_LEN(1) + WAIT_LEN);
+	cs = intel_ring_begin(rq, SESSION_TERMINATION_LEN(hweight32(mask)) + WAIT_LEN);
 	if (IS_ERR(cs)) {
 		err = PTR_ERR(cs);
 		goto out_rq;
 	}
 
-	cs = pxp_emit_session_termination(cs, id);
+	for_each_set_bit(idx, &mask, INTEL_PXP_MAX_HWDRM_SESSIONS)
+		cs = pxp_emit_session_termination(cs, idx);
 	cs = pxp_emit_wait(cs);
 
 	intel_ring_advance(rq, cs);
@@ -138,4 +140,3 @@ int intel_pxp_terminate_session(struct intel_pxp *pxp, u32 id)
 
 	return err;
 }
-
diff --git a/drivers/gpu/drm/i915/pxp/intel_pxp_cmd.h b/drivers/gpu/drm/i915/pxp/intel_pxp_cmd.h
--- a/drivers/gpu/drm/i915/pxp/intel_pxp_cmd.h
+++ b/drivers/gpu/drm/i915/pxp/intel_pxp_cmd.h
@@ -6,10 +6,16 @@
 #ifndef __INTEL_PXP_CMD_H__
 #define __INTEL_PXP_CMD_H__
 
+#include <linux/bits.h>
 #include <linux/types.h>
 
 struct intel_pxp;
 
-int intel_pxp_terminate_session(struct intel_pxp *pxp, u32 idx);
+int intel_pxp_terminate_sessions(struct intel_pxp *pxp, long mask);
+
+static inline int intel_pxp_terminate_session(struct intel_pxp *pxp, u32 id)
+{
+	return intel_pxp_terminate_sessions(pxp, BIT(id));
+}
 
 #endif /* __INTEL_PXP_CMD_H__ */
diff --git a/drivers/gpu/drm/i915/pxp/intel_pxp_pm.c b/drivers/gpu/drm/i915/pxp/intel_pxp_pm.c
--- a/drivers/gpu/drm/i915/pxp/intel_pxp_pm.c
+++ b/drivers/gpu/drm/i915/pxp/intel_pxp_pm.c
@@ -13,7 +13,7 @@ void intel_pxp_suspend(struct intel_pxp *pxp, bool runtime)
 	if (!intel_pxp_is_enabled(pxp))
 		return;
 
-	pxp->arb_is_valid = false;
+	pxp->arb_session.is_valid = false;
 
 	intel_pxp_fini_hw(pxp);
 
diff --git a/drivers/gpu/drm/i915/pxp/intel_pxp_session.c b/drivers/gpu/drm/i915/pxp/intel_pxp_session.c
--- a/drivers/gpu/drm/i915/pxp/intel_pxp_session.c
+++ b/drivers/gpu/drm/i915/pxp/intel_pxp_session.c
@@ -12,14 +12,15 @@
 #include "intel_pxp_tee.h"
 #include "intel_pxp_types.h"
 
-#define ARB_SESSION I915_PROTECTED_CONTENT_DEFAULT_SESSION /* shorter define */
-
 #define GEN12_KCR_SIP _MMIO(0x32260) /* KCR hwdrm session in play 0-31 */
 
+#define KCR_STATUS_1   _MMIO(0x320f4)
+#define KCR_STATUS_1_ATTACK_MASK 0x80000000
+
 /* PXP global terminate register for session termination */
 #define PXP_GLOBAL_TERMINATE _MMIO(0x320f8)
 
-static bool intel_pxp_session_is_in_play(struct intel_pxp *pxp, u32 id)
+bool intel_pxp_session_is_in_play(struct intel_pxp *pxp, u32 id)
 {
 	struct intel_uncore *uncore = pxp_to_gt(pxp)->uncore;
 	intel_wakeref_t wakeref;
@@ -55,12 +56,192 @@ static int pxp_wait_for_session_state(struct intel_pxp *pxp, u32 id, bool in_pla
 	return ret;
 }
 
+/**
+ * is_hwdrm_session_attacked - To check if hwdrm active sessions are attacked.
+ * @pxp: pointer pxp struct
+ *
+ * Return: true if hardware sessions is attacked, false otherwise.
+ */
+static bool is_hwdrm_session_attacked(struct intel_pxp *pxp)
+{
+	u32 regval = 0;
+	intel_wakeref_t wakeref;
+	struct intel_gt *gt = pxp_to_gt(pxp);
+
+	if (pxp->hw_state_invalidated)
+		return true;
+
+	with_intel_runtime_pm(gt->uncore->rpm, wakeref)
+		regval = intel_uncore_read(gt->uncore, KCR_STATUS_1);
+
+	return regval & KCR_STATUS_1_ATTACK_MASK;
+}
+
+static void __init_session_entry(struct intel_pxp_session *session,
+				 struct drm_file *drmfile,
+				 int protection_mode, int session_index)
+{
+	session->protection_mode = protection_mode;
+	session->index = session_index;
+	session->is_valid = false;
+	session->drmfile = drmfile;
+}
+
+/**
+ * create_session_entry - Create a new session entry with provided info.
+ * @pxp: pointer to pxp struct
+ * @drmfile: pointer to drm_file
+ * @session_index: Numeric session identifier.
+ *
+ * Return: status. 0 means creation is successful.
+ */
+static int create_session_entry(struct intel_pxp *pxp, struct drm_file *drmfile,
+				int protection_mode, int session_index)
+{
+	struct intel_pxp_session *session = NULL;
+
+	if (pxp->hwdrm_sessions[session_index])
+		return -EEXIST;
+
+	session = kzalloc(sizeof(*session), GFP_KERNEL);
+	if (!session)
+		return -ENOMEM;
+
+	__init_session_entry(session, drmfile, protection_mode, session_index);
+
+	pxp->hwdrm_sessions[session_index] = session;
+	set_bit(session_index, pxp->reserved_sessions);
+
+	return 0;
+}
+
+static void free_session_entry(struct intel_pxp *pxp, int session_index)
+{
+	if (!pxp->hwdrm_sessions[session_index])
+		return;
+
+	clear_bit(session_index, pxp->reserved_sessions);
+	kfree(fetch_and_zero(&pxp->hwdrm_sessions[session_index]));
+}
+
+void intel_pxp_init_arb_session(struct intel_pxp *pxp)
+{
+	__init_session_entry(&pxp->arb_session, NULL, DOWNSTREAM_DRM_I915_PXP_MODE_HM, ARB_SESSION);
+	pxp->hwdrm_sessions[ARB_SESSION] = &pxp->arb_session;
+	set_bit(ARB_SESSION, pxp->reserved_sessions);
+}
+
+void intel_pxp_fini_arb_session(struct intel_pxp *pxp)
+{
+	pxp->hwdrm_sessions[ARB_SESSION] = NULL;
+	clear_bit(ARB_SESSION, pxp->reserved_sessions);
+}
+
+/**
+ * intel_pxp_sm_ioctl_reserve_session - To reserve an available protected session.
+ * @pxp: pointer to pxp struct
+ * @drmfile: pointer to drm_file.
+ * @pxp_tag: Numeric session identifier returned back to caller.
+ *
+ * Return: status. 0 means reserve is successful.
+ */
+int intel_pxp_sm_ioctl_reserve_session(struct intel_pxp *pxp, struct drm_file *drmfile,
+				       int protection_mode, u32 *pxp_tag)
+{
+	int ret;
+	int idx = 0;
+
+	if (!drmfile || !pxp_tag)
+		return -EINVAL;
+
+	lockdep_assert_held(&pxp->session_mutex);
+
+	/* check if sessions are under attack. if so, don't allow creation */
+	if (is_hwdrm_session_attacked(pxp))
+		return -EPERM;
+
+	if (protection_mode < DOWNSTREAM_DRM_I915_PXP_MODE_LM ||
+	    protection_mode > DOWNSTREAM_DRM_I915_PXP_MODE_SM)
+		return -EINVAL;
+
+	idx = find_first_zero_bit(pxp->reserved_sessions,
+				  INTEL_PXP_MAX_HWDRM_SESSIONS);
+	if (idx >= INTEL_PXP_MAX_HWDRM_SESSIONS)
+		return DOWNSTREAM_DRM_I915_PXP_OP_STATUS_SESSION_NOT_AVAILABLE;
+
+	ret = pxp_wait_for_session_state(pxp, idx, false);
+	if (ret)
+		return DOWNSTREAM_DRM_I915_PXP_OP_STATUS_RETRY_REQUIRED;
+
+	ret = create_session_entry(pxp, drmfile,
+				   protection_mode, idx);
+	*pxp_tag = idx;
+
+	return ret;
+}
+
+/**
+ * intel_pxp_sm_ioctl_terminate_session - To terminate an active HW session and free its entry.
+ * @pxp: pointer to pxp struct.
+ * @drmfile: drm_file of the app issuing the termination
+ * @session_id: Session identifier of the session, containing type and index info
+ *
+ * Return: 0 means terminate is successful, or didn't find the desired session.
+ */
+int intel_pxp_sm_ioctl_terminate_session(struct intel_pxp *pxp,
+					 struct drm_file *drmfile,
+					 int session_id)
+{
+	int ret;
+
+	lockdep_assert_held(&pxp->session_mutex);
+
+	if (!pxp->hwdrm_sessions[session_id])
+		return 0;
+
+	if (pxp->hwdrm_sessions[session_id]->drmfile != drmfile)
+		return -EPERM;
+
+	ret = intel_pxp_terminate_session(pxp, session_id);
+	if (ret)
+		return ret;
+
+	free_session_entry(pxp, session_id);
+
+	return 0;
+}
+
+/**
+ * intel_pxp_sm_ioctl_mark_session_in_play - Put an reserved session to "in_play" state
+ * @pxp: pointer to pxp struct
+ * @drmfile: drm_file of the app marking the session as in play
+ * @session_id: Session identifier of the session, containing type and index info
+ *
+ * Return: status. 0 means update is successful.
+ */
+int intel_pxp_sm_ioctl_mark_session_in_play(struct intel_pxp *pxp,
+					    struct drm_file *drmfile,
+					    u32 session_id)
+{
+	lockdep_assert_held(&pxp->session_mutex);
+
+	if (!pxp->hwdrm_sessions[session_id])
+		return -EINVAL;
+
+	if (pxp->hwdrm_sessions[session_id]->drmfile != drmfile)
+		return -EPERM;
+
+	pxp->hwdrm_sessions[session_id]->is_valid = true;
+
+	return 0;
+}
+
 static int pxp_create_arb_session(struct intel_pxp *pxp)
 {
 	struct intel_gt *gt = pxp_to_gt(pxp);
 	int ret;
 
-	pxp->arb_is_valid = false;
+	pxp->arb_session.is_valid = false;
 
 	if (intel_pxp_session_is_in_play(pxp, ARB_SESSION)) {
 		drm_err(&gt->i915->drm, "arb session already in play at creation time\n");
@@ -82,34 +263,71 @@ static int pxp_create_arb_session(struct intel_pxp *pxp)
 	if (!++pxp->key_instance)
 		++pxp->key_instance;
 
-	pxp->arb_is_valid = true;
+	pxp->arb_session.is_valid = true;
 
 	return 0;
 }
 
-static int pxp_terminate_arb_session_and_global(struct intel_pxp *pxp)
+static int pxp_terminate_all_sessions(struct intel_pxp *pxp)
+{
+	int ret;
+	u32 idx;
+	long mask = 0;
+
+	if (!intel_pxp_is_enabled(pxp))
+		return 0;
+
+	lockdep_assert_held(&pxp->session_mutex);
+
+	for_each_set_bit(idx, pxp->reserved_sessions, INTEL_PXP_MAX_HWDRM_SESSIONS) {
+		pxp->hwdrm_sessions[idx]->is_valid = false;
+		mask |= BIT(idx);
+	}
+
+	if (mask) {
+		ret = intel_pxp_terminate_sessions(pxp, mask);
+		if (ret)
+			return ret;
+	}
+
+	for_each_set_bit(idx, pxp->reserved_sessions, INTEL_PXP_MAX_HWDRM_SESSIONS) {
+		/* we don't want to free the arb session! */
+		if (idx == ARB_SESSION)
+			continue;
+
+		free_session_entry(pxp, idx);
+	}
+
+	return 0;
+}
+
+static int pxp_terminate_all_sessions_and_global(struct intel_pxp *pxp)
 {
 	int ret;
 	struct intel_gt *gt = pxp_to_gt(pxp);
 
 	/* must mark termination in progress calling this function */
-	GEM_WARN_ON(pxp->arb_is_valid);
+	GEM_WARN_ON(pxp->arb_session.is_valid);
+
+	mutex_lock(&pxp->session_mutex);
 
 	/* terminate the hw sessions */
-	ret = intel_pxp_terminate_session(pxp, ARB_SESSION);
+	ret = pxp_terminate_all_sessions(pxp);
 	if (ret) {
 		drm_err(&gt->i915->drm, "Failed to submit session termination\n");
-		return ret;
+		goto out;
 	}
 
 	ret = pxp_wait_for_session_state(pxp, ARB_SESSION, false);
 	if (ret) {
 		drm_err(&gt->i915->drm, "Session state did not clear\n");
-		return ret;
+		goto out;
 	}
 
 	intel_uncore_write(gt->uncore, PXP_GLOBAL_TERMINATE, 1);
 
+out:
+	mutex_unlock(&pxp->session_mutex);
 	return ret;
 }
 
@@ -119,12 +337,13 @@ static void pxp_terminate(struct intel_pxp *pxp)
 
 	pxp->hw_state_invalidated = true;
 
+	ret = pxp_terminate_all_sessions_and_global(pxp);
+
 	/*
 	 * if we fail to submit the termination there is no point in waiting for
 	 * it to complete. PXP will be marked as non-active until the next
 	 * termination is issued.
 	 */
-	ret = pxp_terminate_arb_session_and_global(pxp);
 	if (ret)
 		complete_all(&pxp->termination);
 }
diff --git a/drivers/gpu/drm/i915/pxp/intel_pxp_session.h b/drivers/gpu/drm/i915/pxp/intel_pxp_session.h
--- a/drivers/gpu/drm/i915/pxp/intel_pxp_session.h
+++ b/drivers/gpu/drm/i915/pxp/intel_pxp_session.h
@@ -9,6 +9,24 @@
 #include <linux/types.h>
 
 struct work_struct;
+struct intel_pxp;
+struct drm_file;
+
+#define ARB_SESSION I915_PROTECTED_CONTENT_DEFAULT_SESSION /* shorter define */
+
+void intel_pxp_init_arb_session(struct intel_pxp *pxp);
+void intel_pxp_fini_arb_session(struct intel_pxp *pxp);
+
+int intel_pxp_sm_ioctl_reserve_session(struct intel_pxp *pxp, struct drm_file *drmfile,
+				       int protection_mode, u32 *pxp_tag);
+int intel_pxp_sm_ioctl_mark_session_in_play(struct intel_pxp *pxp,
+					    struct drm_file *drmfile,
+					    u32 session_id);
+int intel_pxp_sm_ioctl_terminate_session(struct intel_pxp *pxp,
+					 struct drm_file *drmfile,
+					 int session_id);
+
+bool intel_pxp_session_is_in_play(struct intel_pxp *pxp, u32 id);
 
 void intel_pxp_session_work(struct work_struct *work);
 
diff --git a/drivers/gpu/drm/i915/pxp/intel_pxp_types.h b/drivers/gpu/drm/i915/pxp/intel_pxp_types.h
--- a/drivers/gpu/drm/i915/pxp/intel_pxp_types.h
+++ b/drivers/gpu/drm/i915/pxp/intel_pxp_types.h
@@ -16,6 +16,28 @@
 struct intel_context;
 struct i915_pxp_component;
 
+#define INTEL_PXP_MAX_HWDRM_SESSIONS 16
+
+struct intel_pxp_session {
+	/** @index: Numeric identifier for this protected session */
+	int index;
+	/** @protection_type: type of protection requested */
+	int protection_type;
+	/** @protection_mode: mode of protection requested */
+	int protection_mode;
+	/** @drmfile: pointer to drm_file, which is allocated on device file open() call */
+	struct drm_file *drmfile;
+
+	/**
+	 * @is_valid: indicates whether the session has been established
+	 *            in the HW root of trust. Note that, after a teardown, the
+	 *            session can still be considered in play on the HW even if
+	 *            the keys are gone, so we can't rely on the HW state of the
+	 *            session to know if it's valid.
+	 */
+	bool is_valid;
+};
+
 /**
  * struct intel_pxp - pxp state
  */
@@ -37,13 +59,6 @@ struct intel_pxp {
 
 	/** @arb_mutex: protects arb session start */
 	struct mutex arb_mutex;
-	/**
-	 * @arb_is_valid: tracks arb session status.
-	 * After a teardown, the arb session can still be in play on the HW
-	 * even if the keys are gone, so we can't rely on the HW state of the
-	 * session to know if it's valid and need to track the status in SW.
-	 */
-	bool arb_is_valid;
 
 	/**
 	 * @key_instance: tracks which key instance we're on, so we can use it
@@ -71,6 +86,11 @@ struct intel_pxp {
 	 */
 	struct completion termination;
 
+	struct mutex session_mutex;
+	DECLARE_BITMAP(reserved_sessions, INTEL_PXP_MAX_HWDRM_SESSIONS);
+	struct intel_pxp_session *hwdrm_sessions[INTEL_PXP_MAX_HWDRM_SESSIONS];
+	struct intel_pxp_session arb_session;
+
 	/** @session_work: worker that manages session events. */
 	struct work_struct session_work;
 	/** @session_events: pending session events, protected with gt->irq_lock. */
-- 
2.33.0.1079.g6e70778dc9-goog


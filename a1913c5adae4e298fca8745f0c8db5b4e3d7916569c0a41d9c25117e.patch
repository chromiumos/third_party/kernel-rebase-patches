From cd81716ab80e4065448c847d118013b748f1796e Mon Sep 17 00:00:00 2001
From: Jie Jiang <jiejiang@chromium.org>
Date: Mon, 11 May 2020 15:30:47 +0900
Subject: [PATCH] CHROMIUM: net: Add per-netns dummy tcp_default_init_rwnd

Android P CTS test checks if it can read this tcp_default_init_rwnd,
and this change adds it to make that test able to pass. See the TEST
line below for the affected test.

tcp_default_init_rwnd is introduced into Android's kernel in
https://r.android.com/79299 and is discarded from 4.19 (see
https://r.android.com/804152). But we still need to support Android P
with kernel 5.4 in CrOS. Since the kernel no longer relies on this
value to set initial congestion window, a simple dummy handler is enough
to pass CTS test.

This change replaces the following two changes in our previous kernel:
- I386b2a9d62de0ebe05c1ebe1b4bd91b314af5c54: the ANDROID change that
adds tcp_default_init_rwnd in sysctl;
- I24482a4d4bfd6229293f4ddf457fa924cf0b01a3: the CHROMIUM change that
makes tcp_default_init_rwnd network namespace aware.

This change can be reverted when all CrOS devices running 5.4 have
updated to ARCVM.

BUG=b:154051922
TEST=cts-tradefed run commandAndExit cts -m CtsPermissionTestCases -t
  android.permission.cts.FileSystemPermissionTest#testTcpDefaultRwndSane
  on a careena with grunt-kernelnext passed.

Signed-off-by: Jie Jiang <jiejiang@chromium.org>
Change-Id: Ifbc056aba0f60845c192d360308f43776261f3f7
Reviewed-on: https://chromium-review.googlesource.com/c/chromiumos/third_party/kernel/+/2192644
Reviewed-by: Hugo Benichi <hugobenichi@google.com>
[rebase510(groeck):
    Squashed:
	CHROMIUM: net: Fix hang caused by dummy tcp_default_init_rwnd
]
Signed-off-by: Guenter Roeck <groeck@chromium.org>
---
 include/net/netns/ipv4.h   |  1 +
 net/ipv4/sysctl_net_ipv4.c | 15 +++++++++++++++
 2 files changed, 16 insertions(+)

diff --git a/include/net/netns/ipv4.h b/include/net/netns/ipv4.h
index f6af8d96d3c6..0f8ebf5db502 100644
--- a/include/net/netns/ipv4.h
+++ b/include/net/netns/ipv4.h
@@ -112,6 +112,7 @@ struct netns_ipv4 {
 #ifdef CONFIG_NET_L3_MASTER_DEV
 	u8 sysctl_tcp_l3mdev_accept;
 #endif
+	int sysctl_tcp_default_init_rwnd;
 	u8 sysctl_tcp_mtu_probing;
 	int sysctl_tcp_mtu_probe_floor;
 	int sysctl_tcp_base_mss;
diff --git a/net/ipv4/sysctl_net_ipv4.c b/net/ipv4/sysctl_net_ipv4.c
index a62934b9f15a..bc539cc85008 100644
--- a/net/ipv4/sysctl_net_ipv4.c
+++ b/net/ipv4/sysctl_net_ipv4.c
@@ -215,6 +215,14 @@ static int ipv4_fwd_update_priority(struct ctl_table *table, int write,
 	return ret;
 }
 
+/* The current kernel does not rely on this value so we do nothing here */
+static int proc_tcp_default_init_rwnd(struct ctl_table *ctl, int write,
+				      void __user *buffer,
+				      size_t *lenp, loff_t *ppos)
+{
+	return proc_dointvec(ctl, write, buffer, lenp, ppos);
+}
+
 static int proc_tcp_congestion_control(struct ctl_table *ctl, int write,
 				       void *buffer, size_t *lenp, loff_t *ppos)
 {
@@ -1368,6 +1376,13 @@ static struct ctl_table ipv4_net_table[] = {
 		.extra1		= SYSCTL_ZERO,
 		.extra2		= &two,
 	},
+	{
+		.procname       = "tcp_default_init_rwnd",
+		.data           = &init_net.ipv4.sysctl_tcp_default_init_rwnd,
+		.maxlen         = sizeof(int),
+		.mode           = 0644,
+		.proc_handler   = proc_tcp_default_init_rwnd
+	},
 	{ }
 };
 
-- 
2.17.1


From 1877c56937afad2a248bc4d1ba226e0d178fad01 Mon Sep 17 00:00:00 2001
From: Hugo Benichi <hugobenichi@google.com>
Date: Wed, 29 Aug 2018 16:29:50 +0900
Subject: [PATCH] CHROMIUM: net: xfrm: expose xfrm_acq_expires sysctl to
 namespaces

Android P now requires as per CTS that the sysctl
/proc/sys/net/core/xfrm_acq_expires be accessible by Android.

More precisely Android P requires to be able to set the value of that
sysctl to 1 hour as the Android IpSec api assumes a long timeout value.
By default xfrm_ac_expires it set to 30 seconds.

The xfrm sysctl handlers defined in net/xfrm/xfrm_sysctl.c are namespace
aware so that xfrm_acq_expires can bet set independently for the host
and for the Android container, but by default Linux will not make any of
the xfrm sysctl available to anything but the init user namespace, which
prevents Android running in the ARC container to write its setting.

This patch resolves this conflict by exposing xfrm_acq_expires to
so that Android running in the ARC container can write to this sysctl.

BUG=b:111578565
BUG=b:112641692
BUG=b:77868515
TEST=- Compiled, flashed, booted eve-arcnext.
     - Verified that /proc/sys/net/core/xfrm_acq_expires is visible
     inside the Android container and defined to be 3600 as per Android
     system/core/rootdir/init.rc
     - Verified that /proc/sys/net/core/xfrm_acq_expires is still the
     default 30 in the root ns.
     - Verified that writing to /proc/sys/net/core/xfrm_acq_expires on
     one side does not change the value on the other side, both ways.

Change-Id: I548117d46f458d4ff2faf9bc53da678c0d63b331
Signed-off-by: Hugo Benichi <hugobenichi@chromium.org>
Reviewed-on: https://chromium-review.googlesource.com/1198622
Commit-Ready: Hugo Benichi <hugobenichi@google.com>
Tested-by: Hugo Benichi <hugobenichi@google.com>
Reviewed-by: Guenter Roeck <groeck@chromium.org>
Kcr-patch: 138ff6f783f5dba83101f4a3f430301383c4ae5577822d20ce6e67a3.patch
---
 net/xfrm/xfrm_sysctl.c | 7 +++++--
 1 file changed, 5 insertions(+), 2 deletions(-)

diff --git a/net/xfrm/xfrm_sysctl.c b/net/xfrm/xfrm_sysctl.c
index 7fdeafc838a7c62539d4ca1b4f6c7b233c1dff57..85e1b6032090ffe0034a305a5fbe4710f4195970 100644
--- a/net/xfrm/xfrm_sysctl.c
+++ b/net/xfrm/xfrm_sysctl.c
@@ -56,9 +56,12 @@ int __net_init xfrm_sysctl_init(struct net *net)
 	table[2].data = &net->xfrm.sysctl_larval_drop;
 	table[3].data = &net->xfrm.sysctl_acq_expires;
 
-	/* Don't export sysctls to unprivileged users */
+	/* Only export xfrm_acq_expires to unprivileged users. This is required
+	 * By Android Ipsec stack as per CTS.
+	 */
 	if (net->user_ns != &init_user_ns) {
-		table[0].procname = NULL;
+		table[0] = table[3];
+		table[1].procname = NULL;
 		table_size = 0;
 	}
 
-- 
2.43.0.472.g3155946c3a-goog


From 50d95074745d1149875388ba2a939055ba9daa32 Mon Sep 17 00:00:00 2001
From: Abhishek Pandit-Subedi <abhishekpandit@chromium.org>
Date: Fri, 9 Oct 2020 13:32:09 -0700
Subject: [PATCH] FROMLIST: Bluetooth: hci_h5: Add driver capabilities for
 RTL8822CS

Certain controller capabilities must be exposed by the driver because it
can't be queried from HCI (wideband speech support, for example). Update
the match data structure to set the supported capabilities and set the
proper quirks on hdev after registering the device.

Also update the 8822CS capabilities to show it supports wideband speech
and has valid le states (allows central peripheral role).

Signed-off-by: Abhishek Pandit-Subedi <abhishekpandit@chromium.org>
(am from https://lore.kernel.org/patchwork/patch/1319254/)
(also found at https://lore.kernel.org/r/20201009133147.1.Ie792480ac24829a48669e83c0045157eb3d46775@changeid)

BUG=b:171926803
TEST=Verified it didn't break Blooglet or Ezkinil

Change-Id: Ie792480ac24829a48669e83c0045157eb3d46775
Signed-off-by: Guenter Roeck <groeck@chromium.org>
---
 drivers/bluetooth/hci_h5.c | 18 +++++++++++++++++-
 1 file changed, 17 insertions(+), 1 deletion(-)

diff --git a/drivers/bluetooth/hci_h5.c b/drivers/bluetooth/hci_h5.c
--- a/drivers/bluetooth/hci_h5.c
+++ b/drivers/bluetooth/hci_h5.c
@@ -102,6 +102,11 @@ struct h5 {
 	struct gpio_desc *device_wake_gpio;
 };
 
+enum h5_capabilities {
+	H5_CAP_WIDEBAND_SPEECH = BIT(0),
+	H5_CAP_VALID_LE_STATES = BIT(1),
+};
+
 enum h5_driver_info {
 	H5_INFO_WAKEUP_DISABLE = BIT(0),
 };
@@ -116,6 +121,7 @@ struct h5_vnd {
 };
 
 struct h5_device_data {
+	uint32_t capabilities;
 	uint32_t driver_info;
 	struct h5_vnd *vnd;
 };
@@ -812,6 +818,7 @@ static const struct hci_uart_proto h5p = {
 static int h5_serdev_probe(struct serdev_device *serdev)
 {
 	struct device *dev = &serdev->dev;
+	struct hci_dev *hdev;
 	struct h5 *h5;
 	const struct h5_device_data *data;
 	int err;
@@ -846,7 +853,6 @@ static int h5_serdev_probe(struct serdev_device *serdev)
 		h5->vnd = data->vnd;
 	}
 
-
 	h5->enable_gpio = devm_gpiod_get_optional(dev, "enable", GPIOD_OUT_LOW);
 	if (IS_ERR(h5->enable_gpio))
 		return PTR_ERR(h5->enable_gpio);
@@ -863,6 +869,15 @@ static int h5_serdev_probe(struct serdev_device *serdev)
 	if (data->driver_info & H5_INFO_WAKEUP_DISABLE)
 		set_bit(H5_WAKEUP_DISABLE, &h5->flags);
 
+	hdev = h5->serdev_hu.hdev;
+
+	/* Set match specific quirks */
+	if (data->capabilities & H5_CAP_WIDEBAND_SPEECH)
+		set_bit(HCI_QUIRK_WIDEBAND_SPEECH_SUPPORTED, &hdev->quirks);
+
+	if (data->capabilities & H5_CAP_VALID_LE_STATES)
+		set_bit(HCI_QUIRK_VALID_LE_STATES, &hdev->quirks);
+
 	return 0;
 }
 
@@ -1063,6 +1078,7 @@ static struct h5_vnd rtl_vnd = {
 };
 
 static const struct h5_device_data h5_data_rtl8822cs = {
+	.capabilities = H5_CAP_WIDEBAND_SPEECH | H5_CAP_VALID_LE_STATES,
 	.vnd = &rtl_vnd,
 };
 
-- 
2.33.0.464.g1972c5931b-goog


From d880d918c925617f1ad38894f379d8ed7a4ce583 Mon Sep 17 00:00:00 2001
From: Norman Bintang <normanbt@chromium.org>
Date: Thu, 20 Jul 2023 16:11:42 +0800
Subject: [PATCH] CHROMIUM: Add xrun counter for snd_pcm_substream

The counter will be available behind CONFIG_SND_PCM_XRUN_DEBUG config.

$ cat /proc/asound/card0/pcm9p/sub0/status

owner_pid   : 1425
trigger_time: 235.248957291
tstamp      : 0.000000000
delay       : 1912
avail       : 480
avail_max   : 1920
-----
hw_ptr      : 672000
appl_ptr    : 673440
xrun_counter: 3  # (new row)

UPSTREAM-TASK=b:292437603
BUG=b:286479507
TEST=cat /proc/asound/card0/pcm0p/sub0/status

Change-Id: Iace538761beb0f9a0b7d630b0a5937a7d7e156c1
Signed-off-by: Norman Bintang <normanbt@chromium.org>
Reviewed-on: https://chromium-review.googlesource.com/c/chromiumos/third_party/kernel/+/4701811
Commit-Queue: Chih-Yang Hsia <paulhsia@chromium.org>
Reviewed-by: Chih-Yang Hsia <paulhsia@chromium.org>
Tested-by: Chih-Yang Hsia <paulhsia@chromium.org>
Reviewed-by: David Riley <davidriley@chromium.org>
---
 include/sound/pcm.h  | 3 +++
 sound/core/pcm.c     | 6 ++++++
 sound/core/pcm_lib.c | 3 +++
 3 files changed, 12 insertions(+)

diff --git a/include/sound/pcm.h b/include/sound/pcm.h
index 210096f124eed5765960b2b512954fb3dfa5c4ec..929412dbc5cd51125d038b92e3a90e49d9a81921 100644
--- a/include/sound/pcm.h
+++ b/include/sound/pcm.h
@@ -492,6 +492,9 @@ struct snd_pcm_substream {
 	/* misc flags */
 	unsigned int hw_opened: 1;
 	unsigned int managed_buffer_alloc:1;
+#ifdef CONFIG_SND_PCM_XRUN_DEBUG
+	unsigned int xrun_counter; /* number of times xrun happens */
+#endif /* CONFIG_SND_PCM_XRUN_DEBUG */
 };
 
 #define SUBSTREAM_BUSY(substream) ((substream)->ref_count > 0)
diff --git a/sound/core/pcm.c b/sound/core/pcm.c
index dc37f3508dc7a04c33d7471293cd9e22ffba66c4..290690fc2abcbbf7cfc4ffcbcd4769d281e64808 100644
--- a/sound/core/pcm.c
+++ b/sound/core/pcm.c
@@ -462,6 +462,9 @@ static void snd_pcm_substream_proc_status_read(struct snd_info_entry *entry,
 	snd_iprintf(buffer, "-----\n");
 	snd_iprintf(buffer, "hw_ptr      : %ld\n", runtime->status->hw_ptr);
 	snd_iprintf(buffer, "appl_ptr    : %ld\n", runtime->control->appl_ptr);
+#ifdef CONFIG_SND_PCM_XRUN_DEBUG
+	snd_iprintf(buffer, "xrun_counter: %d\n", substream->xrun_counter);
+#endif
 }
 
 #ifdef CONFIG_SND_PCM_XRUN_DEBUG
@@ -970,6 +973,9 @@ int snd_pcm_attach_substream(struct snd_pcm *pcm, int stream,
 	substream->pid = get_pid(task_pid(current));
 	pstr->substream_opened++;
 	*rsubstream = substream;
+#ifdef CONFIG_SND_PCM_XRUN_DEBUG
+	substream->xrun_counter = 0;
+#endif /* CONFIG_SND_PCM_XRUN_DEBUG */
 	return 0;
 }
 
diff --git a/sound/core/pcm_lib.c b/sound/core/pcm_lib.c
index 6f73b3c2c205ee201552fb4fb1b2efbc6a63b1d0..459e2fbf6b0f502245db33cc422129803c08f7c1 100644
--- a/sound/core/pcm_lib.c
+++ b/sound/core/pcm_lib.c
@@ -184,6 +184,9 @@ void __snd_pcm_xrun(struct snd_pcm_substream *substream)
 		pcm_warn(substream->pcm, "XRUN: %s\n", name);
 		dump_stack_on_xrun(substream);
 	}
+#ifdef CONFIG_SND_PCM_XRUN_DEBUG
+	substream->xrun_counter++;
+#endif
 }
 
 #ifdef CONFIG_SND_PCM_XRUN_DEBUG
-- 
2.44.0.478.gd926399ef9-goog


From 95c6ba7b76da790c742c3a7b62d569c782491d84 Mon Sep 17 00:00:00 2001
From: Henry Barnor <hbarnor@chromium.org>
Date: Fri, 27 Sep 2024 16:37:59 -0700
Subject: [PATCH] CHROMIUM: HID: multitouch: Remove redundant SKIP_RESET quirk

Given that mt_reset is now a no-op for non-haptic devices; remove the
redundant SKIP_RESET quirk and all it's uses.

BUG=b:352329398
TEST=Build and test kernel on affected devices
UPSTREAM-TASK=b:298170707

Change-Id: Icc613f05109e5387d8059903f39b65d2641a8409
Signed-off-by: Henry Barnor <hbarnor@chromium.org>
Reviewed-on: https://chromium-review.googlesource.com/c/chromiumos/third_party/kernel/+/5895130
Reviewed-by: Angela Czubak <aczubak@google.com>
Reviewed-by: Kenneth Albanowski <kenalba@google.com>
Reviewed-by: Sean O'Brien <seobrien@chromium.org>
---
 drivers/hid/hid-multitouch.c | 35 -----------------------------------
 1 file changed, 35 deletions(-)

diff --git a/drivers/hid/hid-multitouch.c b/drivers/hid/hid-multitouch.c
index c67bd3af07dcd987870d45ea5e491a433a876ac3..00f8f5b09916b2976eb45fa8d9b624f645e468de 100644
--- a/drivers/hid/hid-multitouch.c
+++ b/drivers/hid/hid-multitouch.c
@@ -77,7 +77,6 @@ MODULE_LICENSE("GPL");
 #define MT_QUIRK_FORCE_MULTI_INPUT	BIT(20)
 #define MT_QUIRK_DISABLE_WAKEUP		BIT(21)
 #define MT_QUIRK_ORIENTATION_INVERT	BIT(22)
-#define MT_QUIRK_SKIP_RESET		BIT(23)
 
 #define MT_INPUTMODE_TOUCHSCREEN	0x02
 #define MT_INPUTMODE_TOUCHPAD		0x03
@@ -211,7 +210,6 @@ static void mt_post_parse(struct mt_device *td, struct mt_application *app);
 #define MT_CLS_WIN_8_DISABLE_WAKEUP		0x0016
 #define MT_CLS_WIN_8_NO_STICKY_FINGERS		0x0017
 #define MT_CLS_WIN_8_FORCE_MULTI_INPUT_NSMU	0x0018
-#define MT_CLS_WIN_8_SKIP_RESET			0x0019
 
 /* vendor specific classes */
 #define MT_CLS_3M				0x0101
@@ -330,15 +328,6 @@ static const struct mt_class mt_classes[] = {
 			MT_QUIRK_CONTACT_CNT_ACCURATE |
 			MT_QUIRK_WIN8_PTP_BUTTONS,
 		.export_all_inputs = true },
-	{ .name = MT_CLS_WIN_8_SKIP_RESET,
-		.quirks = MT_QUIRK_ALWAYS_VALID |
-			MT_QUIRK_IGNORE_DUPLICATES |
-			MT_QUIRK_HOVERING |
-			MT_QUIRK_CONTACT_CNT_ACCURATE |
-			MT_QUIRK_STICKY_FINGERS |
-			MT_QUIRK_WIN8_PTP_BUTTONS |
-			MT_QUIRK_SKIP_RESET,
-		.export_all_inputs = true },
 
 	/*
 	 * vendor specific classes
@@ -998,7 +987,6 @@ static int mt_touch_input_mapping(struct hid_device *hdev, struct hid_input *hi,
 			if ((cls->name == MT_CLS_WIN_8 ||
 			     cls->name == MT_CLS_WIN_8_FORCE_MULTI_INPUT ||
 			     cls->name == MT_CLS_WIN_8_FORCE_MULTI_INPUT_NSMU ||
-			     cls->name == MT_CLS_WIN_8_SKIP_RESET ||
 			     cls->name == MT_CLS_WIN_8_DISABLE_WAKEUP) &&
 				(field->application == HID_DG_TOUCHPAD ||
 				 field->application == HID_DG_TOUCHSCREEN))
@@ -2154,9 +2142,6 @@ static int mt_reset(struct hid_device *hdev)
 	if (!td)
 		return 0;
 
-	if (td->mtclass.quirks & MT_QUIRK_SKIP_RESET)
-		return 0;
-
 	if (td->is_haptic_touchpad) {
 		mt_release_contacts(hdev);
 		mt_set_modes(hdev, HID_LATENCY_NORMAL, true, true);
@@ -2325,14 +2310,6 @@ static const struct hid_device_id mt_devices[] = {
 	{ .driver_data = MT_CLS_WIN_8_FORCE_MULTI_INPUT,
 		HID_DEVICE(BUS_I2C, HID_GROUP_MULTITOUCH_WIN_8,
 			USB_VENDOR_ID_ELAN, 0x3148) },
-	{ .driver_data = MT_CLS_WIN_8_SKIP_RESET,
-		HID_DEVICE(HID_BUS_ANY, HID_GROUP_MULTITOUCH_WIN_8,
-			USB_VENDOR_ID_ELAN, 0x30cd) },
-
-	{ .driver_data = MT_CLS_WIN_8_FORCE_MULTI_INPUT_NSMU,
-		HID_DEVICE(BUS_I2C, HID_GROUP_MULTITOUCH_WIN_8,
-			USB_VENDOR_ID_ELAN, 0x32ae) },
-
 	/* Elitegroup panel */
 	{ .driver_data = MT_CLS_SERIAL,
 		MT_USB_DEVICE(USB_VENDOR_ID_ELITEGROUP,
@@ -2483,18 +2460,6 @@ static const struct hid_device_id mt_devices[] = {
 		MT_USB_DEVICE(USB_VENDOR_ID_PANASONIC,
 			USB_DEVICE_ID_PANABOARD_UBT880) },
 
-	/* PixArt touchpad keeping resetting */
-	{ .driver_data = MT_CLS_WIN_8_SKIP_RESET,
-		HID_DEVICE(HID_BUS_ANY, HID_GROUP_MULTITOUCH_WIN_8,
-			USB_VENDOR_ID_PIXART, 0x0255) },
-	{ .driver_data = MT_CLS_WIN_8_SKIP_RESET,
-		HID_DEVICE(HID_BUS_ANY, HID_GROUP_MULTITOUCH_WIN_8,
-			USB_VENDOR_ID_PIXART, 0x1002) },
-	{ .driver_data = MT_CLS_WIN_8_SKIP_RESET,
-		HID_DEVICE(HID_BUS_ANY, HID_GROUP_MULTITOUCH_WIN_8,
-			USB_VENDOR_ID_PIXART, 0x200d) },
-
-
 	/* PixArt optical touch screen */
 	{ .driver_data = MT_CLS_INRANGE_CONTACTNUMBER,
 		MT_USB_DEVICE(USB_VENDOR_ID_PIXART,
-- 
2.47.0.277.g8800431eea-goog


From cb384d5910cd77a6fa2dc962ca270809bae32172 Mon Sep 17 00:00:00 2001
From: Douglas Anderson <dianders@chromium.org>
Date: Wed, 20 Jan 2021 14:34:10 -0800
Subject: [PATCH] CHROMIUM: Undo arm-smmu-qcom.c parts in Merge 'v5.10.4' into
 chromeos-5.10 and Merge 'v5.10.8' into chromeos-5.10

This reverts 'drivers/iommu/arm/arm-smmu/arm-smmu-qcom.c' to how it
was before those merges to simplify reverting old FROMLIST patches and
picking UPSTREAM ones.

BUG=b:170301044
TEST=Use GPU

Cq-Depend: chromium:2640648
Signed-off-by: Douglas Anderson <dianders@chromium.org>
Change-Id: I4b75c2d3f17993b2700ef77e16f7510610474980
Reviewed-on: https://chromium-review.googlesource.com/c/chromiumos/third_party/kernel/+/2611425
Reviewed-by: Rob Clark <robdclark@chromium.org>
---
 drivers/iommu/arm/arm-smmu/arm-smmu-qcom.c | 87 ----------------------
 drivers/iommu/arm/arm-smmu/arm-smmu-qcom.h |  2 -
 2 files changed, 89 deletions(-)

diff --git a/drivers/iommu/arm/arm-smmu/arm-smmu-qcom.c b/drivers/iommu/arm/arm-smmu/arm-smmu-qcom.c
index 91d404deb1155880c4667e72cca527bcbbeb2008..a590afd5e6a9cab51ce3cd5166ff0c86970a014c 100644
--- a/drivers/iommu/arm/arm-smmu/arm-smmu-qcom.c
+++ b/drivers/iommu/arm/arm-smmu/arm-smmu-qcom.c
@@ -264,91 +264,6 @@ static int qcom_smmu_init_context(struct arm_smmu_domain *smmu_domain,
 	return 0;
 }
 
-static int qcom_smmu_cfg_probe(struct arm_smmu_device *smmu)
-{
-	unsigned int last_s2cr = ARM_SMMU_GR0_S2CR(smmu->num_mapping_groups - 1);
-	struct qcom_smmu *qsmmu = to_qcom_smmu(smmu);
-	u32 reg;
-	u32 smr;
-	int i;
-
-	/*
-	 * With some firmware versions writes to S2CR of type FAULT are
-	 * ignored, and writing BYPASS will end up written as FAULT in the
-	 * register. Perform a write to S2CR to detect if this is the case and
-	 * if so reserve a context bank to emulate bypass streams.
-	 */
-	reg = FIELD_PREP(ARM_SMMU_S2CR_TYPE, S2CR_TYPE_BYPASS) |
-	      FIELD_PREP(ARM_SMMU_S2CR_CBNDX, 0xff) |
-	      FIELD_PREP(ARM_SMMU_S2CR_PRIVCFG, S2CR_PRIVCFG_DEFAULT);
-	arm_smmu_gr0_write(smmu, last_s2cr, reg);
-	reg = arm_smmu_gr0_read(smmu, last_s2cr);
-	if (FIELD_GET(ARM_SMMU_S2CR_TYPE, reg) != S2CR_TYPE_BYPASS) {
-		qsmmu->bypass_quirk = true;
-		qsmmu->bypass_cbndx = smmu->num_context_banks - 1;
-
-		set_bit(qsmmu->bypass_cbndx, smmu->context_map);
-
-		arm_smmu_cb_write(smmu, qsmmu->bypass_cbndx, ARM_SMMU_CB_SCTLR, 0);
-
-		reg = FIELD_PREP(ARM_SMMU_CBAR_TYPE, CBAR_TYPE_S1_TRANS_S2_BYPASS);
-		arm_smmu_gr1_write(smmu, ARM_SMMU_GR1_CBAR(qsmmu->bypass_cbndx), reg);
-	}
-
-	for (i = 0; i < smmu->num_mapping_groups; i++) {
-		smr = arm_smmu_gr0_read(smmu, ARM_SMMU_GR0_SMR(i));
-
-		if (FIELD_GET(ARM_SMMU_SMR_VALID, smr)) {
-			/* Ignore valid bit for SMR mask extraction. */
-			smr &= ~ARM_SMMU_SMR_VALID;
-			smmu->smrs[i].id = FIELD_GET(ARM_SMMU_SMR_ID, smr);
-			smmu->smrs[i].mask = FIELD_GET(ARM_SMMU_SMR_MASK, smr);
-			smmu->smrs[i].valid = true;
-
-			smmu->s2crs[i].type = S2CR_TYPE_BYPASS;
-			smmu->s2crs[i].privcfg = S2CR_PRIVCFG_DEFAULT;
-			smmu->s2crs[i].cbndx = 0xff;
-		}
-	}
-
-	return 0;
-}
-
-static void qcom_smmu_write_s2cr(struct arm_smmu_device *smmu, int idx)
-{
-	struct arm_smmu_s2cr *s2cr = smmu->s2crs + idx;
-	struct qcom_smmu *qsmmu = to_qcom_smmu(smmu);
-	u32 cbndx = s2cr->cbndx;
-	u32 type = s2cr->type;
-	u32 reg;
-
-	if (qsmmu->bypass_quirk) {
-		if (type == S2CR_TYPE_BYPASS) {
-			/*
-			 * Firmware with quirky S2CR handling will substitute
-			 * BYPASS writes with FAULT, so point the stream to the
-			 * reserved context bank and ask for translation on the
-			 * stream
-			 */
-			type = S2CR_TYPE_TRANS;
-			cbndx = qsmmu->bypass_cbndx;
-		} else if (type == S2CR_TYPE_FAULT) {
-			/*
-			 * Firmware with quirky S2CR handling will ignore FAULT
-			 * writes, so trick it to write FAULT by asking for a
-			 * BYPASS.
-			 */
-			type = S2CR_TYPE_BYPASS;
-			cbndx = 0xff;
-		}
-	}
-
-	reg = FIELD_PREP(ARM_SMMU_S2CR_TYPE, type) |
-	      FIELD_PREP(ARM_SMMU_S2CR_CBNDX, cbndx) |
-	      FIELD_PREP(ARM_SMMU_S2CR_PRIVCFG, s2cr->privcfg);
-	arm_smmu_gr0_write(smmu, ARM_SMMU_GR0_S2CR(idx), reg);
-}
-
 static int qcom_smmu_def_domain_type(struct device *dev)
 {
 	const struct of_device_id *match =
@@ -395,10 +310,8 @@ static const struct arm_smmu_impl qcom_smmu_500_impl = {
 
 static const struct arm_smmu_impl sdm845_smmu_500_impl = {
 	.init_context = qcom_smmu_init_context,
-	.cfg_probe = qcom_smmu_cfg_probe,
 	.def_domain_type = qcom_smmu_def_domain_type,
 	.reset = qcom_sdm845_smmu500_reset,
-	.write_s2cr = qcom_smmu_write_s2cr,
 	.tlb_sync = qcom_smmu_tlb_sync,
 };
 
diff --git a/drivers/iommu/arm/arm-smmu/arm-smmu-qcom.h b/drivers/iommu/arm/arm-smmu/arm-smmu-qcom.h
index 593910567b8842c5522351aa1fcf74df46be87a6..252904f6ab5257634ccc1a101b341d7c484a5444 100644
--- a/drivers/iommu/arm/arm-smmu/arm-smmu-qcom.h
+++ b/drivers/iommu/arm/arm-smmu/arm-smmu-qcom.h
@@ -9,8 +9,6 @@
 struct qcom_smmu {
 	struct arm_smmu_device smmu;
 	const struct qcom_smmu_config *cfg;
-	bool bypass_quirk;
-	u8 bypass_cbndx;
 	u32 stall_enabled;
 };
 
-- 
2.39.0.314.g84b9a713c41-goog


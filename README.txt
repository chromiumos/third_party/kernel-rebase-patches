This is the directory in which conflict resolutions are stored.

To enable dependency verification hooks:
> $ cd hooks
> $ install_hooks.sh

To disable dependency verification hooks:
> $ cd hooks
> $ install_hooks.sh remove

From 52be6bafbda2c367103ed427181d90d00f2cb76a Mon Sep 17 00:00:00 2001
From: Abhishek Pandit-Subedi <abhishekpandit@chromium.org>
Date: Fri, 9 Oct 2020 13:32:09 -0700
Subject: [PATCH] FROMLIST: Bluetooth: hci_h5: Add driver capabilities for
 RTL8822CS

Certain controller capabilities must be exposed by the driver because it
can't be queried from HCI (wideband speech support, for example). Update
the match data structure to set the supported capabilities and set the
proper quirks on hdev after registering the device.

Also update the 8822CS capabilities to show it supports wideband speech
and has valid le states (allows central peripheral role).

Signed-off-by: Abhishek Pandit-Subedi <abhishekpandit@chromium.org>
(am from https://lore.kernel.org/patchwork/patch/1319254/)
(also found at https://lore.kernel.org/r/20201009133147.1.Ie792480ac24829a48669e83c0045157eb3d46775@changeid)

BUG=b:171926803
TEST=Verified it didn't break Blooglet or Ezkinil

Change-Id: Ie792480ac24829a48669e83c0045157eb3d46775
[rebase515(groeck): Context conflicts]
Signed-off-by: Guenter Roeck <groeck@chromium.org>
---
 drivers/bluetooth/hci_h5.c | 17 +++++++++++++++++
 1 file changed, 17 insertions(+)

diff --git a/drivers/bluetooth/hci_h5.c b/drivers/bluetooth/hci_h5.c
index c5a0409ef84fd31ff8b9a8fbaf95a0803eab831d..fe679e18f94c755b526f24af09310eccf13cbafd 100644
--- a/drivers/bluetooth/hci_h5.c
+++ b/drivers/bluetooth/hci_h5.c
@@ -102,6 +102,11 @@ struct h5 {
 	struct gpio_desc *device_wake_gpio;
 };
 
+enum h5_capabilities {
+	H5_CAP_WIDEBAND_SPEECH = BIT(0),
+	H5_CAP_VALID_LE_STATES = BIT(1),
+};
+
 enum h5_driver_info {
 	H5_INFO_WAKEUP_DISABLE = BIT(0),
 };
@@ -116,6 +121,7 @@ struct h5_vnd {
 };
 
 struct h5_device_data {
+	uint32_t capabilities;
 	uint32_t driver_info;
 	struct h5_vnd *vnd;
 };
@@ -816,6 +822,7 @@ static const struct hci_uart_proto h5p = {
 static int h5_serdev_probe(struct serdev_device *serdev)
 {
 	struct device *dev = &serdev->dev;
+	struct hci_dev *hdev;
 	struct h5 *h5;
 	const struct h5_device_data *data;
 
@@ -861,6 +868,15 @@ static int h5_serdev_probe(struct serdev_device *serdev)
 	if (IS_ERR(h5->device_wake_gpio))
 		return PTR_ERR(h5->device_wake_gpio);
 
+	hdev = h5->serdev_hu.hdev;
+
+	/* Set match specific quirks */
+	if (data->capabilities & H5_CAP_WIDEBAND_SPEECH)
+		set_bit(HCI_QUIRK_WIDEBAND_SPEECH_SUPPORTED, &hdev->quirks);
+
+	if (data->capabilities & H5_CAP_VALID_LE_STATES)
+		set_bit(HCI_QUIRK_VALID_LE_STATES, &hdev->quirks);
+
 	return hci_uart_register_device(&h5->serdev_hu, &h5p);
 }
 
@@ -1069,6 +1085,7 @@ static struct h5_vnd rtl_vnd = {
 };
 
 static const struct h5_device_data h5_data_rtl8822cs = {
+	.capabilities = H5_CAP_WIDEBAND_SPEECH | H5_CAP_VALID_LE_STATES,
 	.vnd = &rtl_vnd,
 };
 
-- 
2.38.1.584.g0f3c55d4c2-goog


#!/bin/bash
# Copyright 2023 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

remove=0
if [[ "$#" -ne 0 && "$#" -ne 1 ]]; then
    echo "Invalid number of arguments" $#
fi

if [ "$#" -eq 1 ]; then
    if [ "$1" == "remove" ]; then
        remove=1
    else
        echo "Invalid argument" $1
	exit -1
    fi
fi

if [ -f ../.git/hooks/commit-msg ]; then
    rm ../.git/hooks/commit-msg
fi

if [ -f ../.git/hooks/prepare-commit-msg ]; then
    rm ../.git/hooks/prepare-commit-msg
fi

if [ $remove -eq 0 ]; then
    ln -s ../../hooks/commit-msg ../.git/hooks/commit-msg
    ln -s ../../hooks/prepare-commit-msg ../.git/hooks/prepare-commit-msg
    echo "Installed git hooks"
else
    echo "Removed git hooks"
fi

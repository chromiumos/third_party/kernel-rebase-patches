From 58981a873b4cba095701d8fc51caa40def3bb0ad Mon Sep 17 00:00:00 2001
From: Archie Pusaka <apusaka@chromium.org>
Date: Fri, 27 Aug 2021 23:26:52 +0800
Subject: [PATCH] CHROMIUM: Disable Remote Wakeup for RTL8822 USB

The parent of this patch enables RTL chips to remote wakeup in order
to let future gens of RTK chips to act as wakeup source. However,
it also enables RTL8822 to do so. Previously, RTL8822 remote wakeup
capability are disabled in order to save power.

This patch keeps remote wakeup for RTL8822 disabled in order to
preserve the condition.

BUG=b:178449733
TEST=verify BT keyboard cannot wake Blooglet from suspend

Signed-off-by: Archie Pusaka <apusaka@chromium.org>
Change-Id: I8bdf506da4c7c575ab69502d44afa246c423d827
Reviewed-on: https://chromium-review.googlesource.com/c/chromiumos/third_party/kernel/+/3161675
Reviewed-by: Abhishek Pandit-Subedi <abhishekpandit@chromium.org>
---
 drivers/bluetooth/btrtl.c   | 5 +++++
 drivers/bluetooth/btusb.c   | 4 +++-
 include/net/bluetooth/hci.h | 5 +++++
 3 files changed, 13 insertions(+), 1 deletion(-)

diff --git a/drivers/bluetooth/btrtl.c b/drivers/bluetooth/btrtl.c
index 47c28fd8f006..c2be73db8c58 100644
--- a/drivers/bluetooth/btrtl.c
+++ b/drivers/bluetooth/btrtl.c
@@ -776,6 +776,11 @@ void btrtl_set_quirks(struct hci_dev *hdev, struct btrtl_device_info *btrtl_dev)
 	/* Enable WBS supported for the specific Realtek devices. */
 	switch (btrtl_dev->project_id) {
 	case CHIP_ID_8822C:
+	       /* Disallow RTL8822 to remote wakeup, in order to enter
+		* global suspend and save power.
+		*/
+		set_bit(HCI_QUIRK_DISABLE_REMOTE_WAKE, &hdev->quirks);
+		fallthrough;
 	case CHIP_ID_8852A:
 	case CHIP_ID_8852B:
 	case CHIP_ID_8852C:
diff --git a/drivers/bluetooth/btusb.c b/drivers/bluetooth/btusb.c
index 9975687ecefd..6fedd2c59fa4 100644
--- a/drivers/bluetooth/btusb.c
+++ b/drivers/bluetooth/btusb.c
@@ -4036,7 +4036,9 @@ static int btusb_suspend(struct usb_interface *intf, pm_message_t message)
 		    device_can_wakeup(&data->udev->dev))
 			data->udev->do_remote_wakeup = 1;
 		else if (!PMSG_IS_AUTO(message) &&
-			 !device_may_wakeup(&data->udev->dev)) {
+			 (!device_may_wakeup(&data->udev->dev) ||
+			  test_bit(HCI_QUIRK_DISABLE_REMOTE_WAKE,
+				   &data->hdev->quirks))) {
 			data->udev->do_remote_wakeup = 0;
 			data->udev->reset_resume = 1;
 		}
diff --git a/include/net/bluetooth/hci.h b/include/net/bluetooth/hci.h
index fe7935be7dc4..af15606bbd2d 100644
--- a/include/net/bluetooth/hci.h
+++ b/include/net/bluetooth/hci.h
@@ -274,6 +274,11 @@ enum {
 	 * during the hdev->setup vendor callback.
 	 */
 	HCI_QUIRK_BROKEN_ENHANCED_SETUP_SYNC_CONN,
+
+	/* CHROMIUM-only: This quirk prevents RTL8822 to perform remote wake
+	 * on system suspend to save power. This shouldn't be upstreamed.
+	 */
+	HCI_QUIRK_DISABLE_REMOTE_WAKE,
 };
 
 /* HCI device flags */
-- 
2.37.0.170.g444d1eabd0-goog


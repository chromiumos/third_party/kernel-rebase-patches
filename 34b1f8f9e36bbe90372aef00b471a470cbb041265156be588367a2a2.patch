From 9c0d2bed392c67153352650a75ae91cf0f3315a7 Mon Sep 17 00:00:00 2001
From: "Joel Fernandes (Google)" <joel@joelfernandes.org>
Date: Sat, 6 May 2023 00:40:00 +0000
Subject: [PATCH] CHROMIUM: tick-sched: Let stop code round to next tick using
 hrtimer_forward()

This prevents a case where the next tick is rounded to TICK_NSEC,
however the tick-sched timer is actually at a non-multiple of TICK_NSEC
value. This causes timer interrupts in quick succession when the tick is
stopped and restarted.

[CHROMIUM note: I am marking it as CHROMIUM for experimentation in Finch
and will push upstream after seeing results.]

BUG=b:263289152
UPSTREAM-TASK=b:289837863
TEST=cyclictest --smp -p95 -m -i 100 -d 100 with timer_highres=0

Signed-off-by: Joel Fernandes (Google) <joel@joelfernandes.org>
Signed-off-by: Vineeth Pillai <vineethrp@google.com>
Change-Id: I03561b7a5b39aabd446fdca5a9b6cd80b692d8ae
Reviewed-on: https://chromium-review.googlesource.com/c/chromiumos/third_party/kernel/+/4855710

[rebase66(tzungbi):
 Revised commit message.
 Squashed:
   FIXUP: CHROMIUM: tick-sched: Let stop code round to next tick using hrtimer_forward()
     (https://crrev.com/c/4889804)
]
Signed-off-by: Tzung-Bi Shih <tzungbi@chromium.org>
---
 kernel/time/tick-sched.c | 13 +++++++++++--
 kernel/time/timer.c      | 14 ++++++++------
 2 files changed, 19 insertions(+), 8 deletions(-)

diff --git a/kernel/time/tick-sched.c b/kernel/time/tick-sched.c
index 88554aa2ae354f1e9798f60559279c310959afe9..216094bde9b6bc4046044def1f32b10c9bcf415a 100644
--- a/kernel/time/tick-sched.c
+++ b/kernel/time/tick-sched.c
@@ -956,8 +956,17 @@ static void tick_nohz_stop_tick(struct tick_sched *ts, int cpu)
 		hrtimer_start(&ts->sched_timer, expires,
 			      HRTIMER_MODE_ABS_PINNED_HARD);
 	} else {
-		hrtimer_set_expires(&ts->sched_timer, expires);
-		tick_program_event(expires, 1);
+		/*
+		 * hrtimer_set_expires() may have previously set the expiry
+		 * well into the future, however an unrelated wakeup + timer
+		 * queuing means now the hrtimer needs to be backtracked, or
+		 * we'll just miss events. Back track it to last_tick, and
+		 * then use hrtimer_forward to forward it past 'tick'.
+		 */
+		hrtimer_set_expires(&ts->sched_timer, ts->last_tick);
+		hrtimer_forward(&ts->sched_timer, expires, TICK_NSEC);
+		ts->next_tick = hrtimer_get_expires(&ts->sched_timer);
+		tick_program_event(ts->next_tick, 1);
 	}
 }
 
diff --git a/kernel/time/timer.c b/kernel/time/timer.c
index 352b161113cda6856a6fac08e9cc5eb01818a056..2a9c118ae1ffff84faa298998488478677038582 100644
--- a/kernel/time/timer.c
+++ b/kernel/time/timer.c
@@ -1901,14 +1901,16 @@ static u64 cmp_next_hrtimer_event(u64 basem, u64 expires)
 		return basem;
 
 	/*
-	 * Round up to the next jiffie. High resolution timers are
-	 * off, so the hrtimers are expired in the tick and we need to
-	 * make sure that this tick really expires the timer to avoid
-	 * a ping pong of the nohz stop code.
+	 * Previously we were rounding up to the next jiffie as when
+	 * high resolution timers are off, the hrtimers are expired in
+	 * the tick and we need to make sure that this tick really expires
+	 * the timer to avoid a ping pong of the nohz stop code.
 	 *
-	 * Use DIV_ROUND_UP_ULL to prevent gcc calling __divdi3
+	 * However, the nohz stop code uses hrtimer_forward() for the
+	 * low res case, which makes sure that we already do a rounding
+	 * there. So need to round to TICK_NSEC here.
 	 */
-	return DIV_ROUND_UP_ULL(nextevt, TICK_NSEC) * TICK_NSEC;
+	return nextevt;
 }
 
 /**
-- 
2.43.0.429.g432eaa2c6b-goog


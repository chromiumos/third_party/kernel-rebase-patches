From 84fdb603d3909e7112a0734b2fb3fc752704142e Mon Sep 17 00:00:00 2001
From: Abhijeet Rao <abhijeet.rao@intel.corp-partner.google.com>
Date: Fri, 18 Mar 2022 02:57:23 +0000
Subject: [PATCH] Reland "FROMLIST: PCI/AER: Disable AER service when link is
 in L2/L3 ready, L2 and L3 state"

This reverts commit b6496724042be76287ecffac97f578f6e8f5a21f.

Reason for revert: The hard hang issue is fixed in CB, hence these patches can be reland

BUG=b:217419041

TEST=Connect Gatkex card and authorize it & run suspend_stress_test -c 10

Signed-off-by: Abhijeet Rao <abhijeet.rao@intel.corp-partner.google.com>

Original change's description:
> Revert "FROMLIST: PCI/AER: Disable AER service when link is in L2/L3 ready, L2 and L3 state"
>
> This reverts commit 127c8e1eefc92fd085f6419eb4ce71ce1dfa3b04.
>
> Reason for revert: Causing regression with Soix, b/220796339
>
> BUG: b/220796339
>
> Original change's description:
> > FROMLIST: PCI/AER: Disable AER service when link is in L2/L3 ready, L2 and L3 state
> >
> > Commit 50310600ebda ("iommu/vt-d: Enable PCI ACS for platform opt in
> > hint") enables ACS, and some platforms lose its NVMe after resume from
> > S3:
> > [   50.947816] pcieport 0000:00:1b.0: DPC: containment event, status:0x1f01 source:0x0000
> > [   50.947817] pcieport 0000:00:1b.0: DPC: unmasked uncorrectable error detected
> > [   50.947829] pcieport 0000:00:1b.0: PCIe Bus Error: severity=Uncorrected (Non-Fatal), type=Transaction Layer, (Receiver ID)
> > [   50.947830] pcieport 0000:00:1b.0:   device [8086:06ac] error status/mask=00200000/00010000
> > [   50.947831] pcieport 0000:00:1b.0:    [21] ACSViol                (First)
> > [   50.947841] pcieport 0000:00:1b.0: AER: broadcast error_detected message
> > [   50.947843] nvme nvme0: frozen state error detected, reset controller
> >
> > It happens right after ACS gets enabled during resume.
> >
> > There's another case, when Thunderbolt reaches D3cold:
> > [   30.100211] pcieport 0000:00:1d.0: AER: Uncorrected (Non-Fatal) error received: 0000:00:1d.0
> > [   30.100251] pcieport 0000:00:1d.0: PCIe Bus Error: severity=Uncorrected (Non-Fatal), type=Transaction Layer, (Requester ID)
> > [   30.100256] pcieport 0000:00:1d.0:   device [8086:7ab0] error status/mask=00100000/00004000
> > [   30.100262] pcieport 0000:00:1d.0:    [20] UnsupReq               (First)
> > [   30.100267] pcieport 0000:00:1d.0: AER:   TLP Header: 34000000 08000052 00000000 00000000
> > [   30.100372] thunderbolt 0000:0a:00.0: AER: can't recover (no error_detected callback)
> > [   30.100401] xhci_hcd 0000:3e:00.0: AER: can't recover (no error_detected callback)
> > [   30.100427] pcieport 0000:00:1d.0: AER: device recovery failed
> >
> > So disable AER service to avoid the noises from turning power rails
> > on/off when the device is in low power states (D3hot and D3cold), as
> > PCIe spec "5.2 Link State Power Management" states that TLP and DLLP
> > transmission is disabled for a Link in L2/L3 Ready (D3hot), L2 (D3cold
> > with aux power) and L3 (D3cold).
> >
> > Bugzilla: https://bugzilla.kernel.org/show_bug.cgi?id=209149
> > Bugzilla: https://bugzilla.kernel.org/show_bug.cgi?id=215453
> > Fixes: 50310600ebda ("iommu/vt-d: Enable PCI ACS for platform opt in hint")
> > Signed-off-by: Kai-Heng Feng <kai.heng.feng@canonical.com>
> > (am from https://patchwork.kernel.org/patch/12726163/)
> > (also found at
> > https://patchwork.kernel.org/project/linux-pci/patch/20220127025418.1989642-1-kai.heng.feng@canonical.com/)
> >
> > BUG=b:217419041
> > TEST=Connect Gatkex card and authorize it & run suspend_stress_test -c 10  . S0ix test passes 10 cycles
> >
> > Signed-off-by: George D Sworo <george.d.sworo@intel.com>
> > Change-Id: Icc67cec1ac85bc645faec742da809946a4f53612
> > Reviewed-on: https://chromium-review.googlesource.com/c/chromiumos/third_party/kernel/+/3437705
> > Reviewed-by: Sean Paul <seanpaul@chromium.org>
> > Reviewed-by: Rajat Jain <rajatja@google.com>
>
> Bug: b:217419041
> Change-Id: I2ca052904867227486c09c1b076f8094c2fba819
> Reviewed-on: https://chromium-review.googlesource.com/c/chromiumos/third_party/kernel/+/3497915
> Reviewed-by: Rajat Jain <rajatja@google.com>
> Tested-by: Rajat Jain <rajatja@google.com>
> Commit-Queue: Rajat Jain <rajatja@google.com>

Bug: b:217419041
Change-Id: I6b2f890583cca27fe6281a86b4140dc542adaa0d
Reviewed-on: https://chromium-review.googlesource.com/c/chromiumos/third_party/kernel/+/3535340
Reviewed-by: Sean Paul <seanpaul@chromium.org>
Reviewed-by: Rajat Jain <rajatja@google.com>
Commit-Queue: Rajat Jain <rajatja@google.com>
Tested-by: Rajat Jain <rajatja@google.com>
(cherry picked from commit 1edbac51a1976e705cc7edcba17b7f0b0005c6d0)
Reviewed-on: https://chromium-review.googlesource.com/c/chromiumos/third_party/kernel/+/3539485
---
 drivers/pci/pcie/aer.c | 17 +++++++++--------
 1 file changed, 9 insertions(+), 8 deletions(-)

diff --git a/drivers/pci/pcie/aer.c b/drivers/pci/pcie/aer.c
index 13b8586924ead15f1fc99d71cfe26d9fc89f4fb4..ff254ceebdcfd5a2018145e8193b6c00016a4212 100644
--- a/drivers/pci/pcie/aer.c
+++ b/drivers/pci/pcie/aer.c
@@ -1572,14 +1572,15 @@ static pci_ers_result_t aer_root_reset(struct pci_dev *dev)
 }
 
 static struct pcie_port_service_driver aerdriver = {
-	.name		= "aer",
-	.port_type	= PCIE_ANY_PORT,
-	.service	= PCIE_PORT_SERVICE_AER,
-
-	.probe		= aer_probe,
-	.suspend	= aer_suspend,
-	.resume		= aer_resume,
-	.remove		= aer_remove,
+	.name			= "aer",
+	.port_type		= PCIE_ANY_PORT,
+	.service		= PCIE_PORT_SERVICE_AER,
+	.probe			= aer_probe,
+	.suspend		= aer_suspend,
+	.resume			= aer_resume,
+	.runtime_suspend	= aer_suspend,
+	.runtime_resume		= aer_resume,
+	.remove			= aer_remove,
 };
 
 /**
-- 
2.46.0.rc2.264.g509ed76dc8-goog


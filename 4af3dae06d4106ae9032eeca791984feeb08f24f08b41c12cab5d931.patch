From c0cabf89caaa8ea00c0c203911baba50b2e94180 Mon Sep 17 00:00:00 2001
From: Sarthak Kukreti <sarthakkukreti@chromium.org>
Date: Mon, 25 Sep 2023 14:22:31 -0700
Subject: [PATCH] CHROMIUM: Revert "ext4: get discard out of jbd2 commit
 kthread contex"

This reverts commit 98381d10b0ae90cdac52e30c258fb3f2b6bc6d7b.

With this patch, large file deletions do not reclaim space on
thin provisioned filesystems (LVM, virtio-blk over sparse files).

BUG=b:288998343
TEST=create large file, delete and unmount
TEST=space is reclaimed before unmounting the filesystem
UPSTREAM-TASK=b:300199695

Change-Id: Ia52876235e643df446556d48e855d16f27ec2606
Signed-off-by: Sarthak Kukreti <sarthakkukreti@chromium.org>
Reviewed-on: https://chromium-review.googlesource.com/c/chromiumos/third_party/kernel/+/5335647
Reviewed-by: David Riley <davidriley@chromium.org>
Commit-Queue: Sarthak Kukreti <sarthakkukreti@google.com>
Tested-by: Sarthak Kukreti <sarthakkukreti@google.com>
---
 fs/ext4/ext4.h    |  2 -
 fs/ext4/mballoc.c | 96 ++++++++++++-----------------------------------
 2 files changed, 25 insertions(+), 73 deletions(-)

diff --git a/fs/ext4/ext4.h b/fs/ext4/ext4.h
index 6accbc7bfb59050fb0eb3d52659792ed98a65a1f..efc04078176b71223d761f05e204206473afdcdb 100644
--- a/fs/ext4/ext4.h
+++ b/fs/ext4/ext4.h
@@ -1579,8 +1579,6 @@ struct ext4_sb_info {
 	unsigned int s_mb_free_pending;
 	struct list_head s_freed_data_list[2];	/* List of blocks to be freed
 						   after commit completed */
-	struct list_head s_discard_list;
-	struct work_struct s_discard_work;
 	struct list_head *s_mb_avg_fragment_size;
 	rwlock_t *s_mb_avg_fragment_size_locks;
 	struct list_head *s_mb_largest_free_orders;
diff --git a/fs/ext4/mballoc.c b/fs/ext4/mballoc.c
index 964fecb5f529f33cecf485a4167cfbe4286d0618..7d89616a4009b6c09abd6daab0d9647e67f5c9a6 100644
--- a/fs/ext4/mballoc.c
+++ b/fs/ext4/mballoc.c
@@ -3521,54 +3521,6 @@ static int ext4_groupinfo_create_slab(size_t size)
 	return 0;
 }
 
-static void ext4_discard_work(struct work_struct *work)
-{
-	struct ext4_sb_info *sbi = container_of(work,
-			struct ext4_sb_info, s_discard_work);
-	struct super_block *sb = sbi->s_sb;
-	struct ext4_free_data *fd, *nfd;
-	struct ext4_buddy e4b;
-	LIST_HEAD(discard_list);
-	ext4_group_t grp, load_grp;
-	int err = 0;
-
-	spin_lock(&sbi->s_md_lock);
-	list_splice_init(&sbi->s_discard_list, &discard_list);
-	spin_unlock(&sbi->s_md_lock);
-
-	load_grp = UINT_MAX;
-	list_for_each_entry_safe(fd, nfd, &discard_list, efd_list) {
-		/*
-		 * If filesystem is umounting or no memory, give up the discard
-		 */
-		if ((sb->s_flags & SB_ACTIVE) && !err) {
-			grp = fd->efd_group;
-			if (grp != load_grp) {
-				if (load_grp != UINT_MAX)
-					ext4_mb_unload_buddy(&e4b);
-
-				err = ext4_mb_load_buddy(sb, grp, &e4b);
-				if (err) {
-					kmem_cache_free(ext4_free_data_cachep, fd);
-					load_grp = UINT_MAX;
-					continue;
-				} else {
-					load_grp = grp;
-				}
-			}
-
-			ext4_lock_group(sb, grp);
-			ext4_try_to_trim_range(sb, &e4b, fd->efd_start_cluster,
-						fd->efd_start_cluster + fd->efd_count - 1, 1);
-			ext4_unlock_group(sb, grp);
-		}
-		kmem_cache_free(ext4_free_data_cachep, fd);
-	}
-
-	if (load_grp != UINT_MAX)
-		ext4_mb_unload_buddy(&e4b);
-}
-
 int ext4_mb_init(struct super_block *sb)
 {
 	struct ext4_sb_info *sbi = EXT4_SB(sb);
@@ -3654,8 +3606,6 @@ int ext4_mb_init(struct super_block *sb)
 	sbi->s_mb_free_pending = 0;
 	INIT_LIST_HEAD(&sbi->s_freed_data_list[0]);
 	INIT_LIST_HEAD(&sbi->s_freed_data_list[1]);
-	INIT_LIST_HEAD(&sbi->s_discard_list);
-	INIT_WORK(&sbi->s_discard_work, ext4_discard_work);
 
 	sbi->s_mb_max_to_scan = MB_DEFAULT_MAX_TO_SCAN;
 	sbi->s_mb_min_to_scan = MB_DEFAULT_MIN_TO_SCAN;
@@ -3757,14 +3707,6 @@ void ext4_mb_release(struct super_block *sb)
 	struct kmem_cache *cachep = get_groupinfo_cache(sb->s_blocksize_bits);
 	int count;
 
-	if (test_opt(sb, DISCARD)) {
-		/*
-		 * wait the discard work to drain all of ext4_free_data
-		 */
-		flush_work(&sbi->s_discard_work);
-		WARN_ON_ONCE(!list_empty(&sbi->s_discard_list));
-	}
-
 	if (sbi->s_group_info) {
 		for (i = 0; i < ngroups; i++) {
 			cond_resched();
@@ -3888,6 +3830,7 @@ static void ext4_free_data_in_buddy(struct super_block *sb,
 		put_page(e4b.bd_bitmap_page);
 	}
 	ext4_unlock_group(sb, entry->efd_group);
+	kmem_cache_free(ext4_free_data_cachep, entry);
 	ext4_mb_unload_buddy(&e4b);
 
 	mb_debug(sb, "freed %d blocks in 1 structures\n", count);
@@ -3901,26 +3844,37 @@ void ext4_process_freed_data(struct super_block *sb, tid_t commit_tid)
 {
 	struct ext4_sb_info *sbi = EXT4_SB(sb);
 	struct ext4_free_data *entry, *tmp;
+	struct bio *discard_bio = NULL;
 	LIST_HEAD(freed_data_list);
 	struct list_head *s_freed_head = &sbi->s_freed_data_list[commit_tid & 1];
-	bool wake;
+	int err;
 
 	list_replace_init(s_freed_head, &freed_data_list);
 
-	list_for_each_entry(entry, &freed_data_list, efd_list)
-		ext4_free_data_in_buddy(sb, entry);
-
 	if (test_opt(sb, DISCARD)) {
-		spin_lock(&sbi->s_md_lock);
-		wake = list_empty(&sbi->s_discard_list);
-		list_splice_tail(&freed_data_list, &sbi->s_discard_list);
-		spin_unlock(&sbi->s_md_lock);
-		if (wake)
-			queue_work(system_unbound_wq, &sbi->s_discard_work);
-	} else {
-		list_for_each_entry_safe(entry, tmp, &freed_data_list, efd_list)
-			kmem_cache_free(ext4_free_data_cachep, entry);
+		list_for_each_entry(entry, &freed_data_list, efd_list) {
+			err = ext4_issue_discard(sb, entry->efd_group,
+						 entry->efd_start_cluster,
+						 entry->efd_count,
+						 &discard_bio);
+			if (err && err != -EOPNOTSUPP) {
+				ext4_msg(sb, KERN_WARNING, "discard request in"
+					 " group:%d block:%d count:%d failed"
+					 " with %d", entry->efd_group,
+					 entry->efd_start_cluster,
+					 entry->efd_count, err);
+			} else if (err == -EOPNOTSUPP)
+				break;
+		}
+
+		if (discard_bio) {
+			submit_bio_wait(discard_bio);
+			bio_put(discard_bio);
+		}
 	}
+
+	list_for_each_entry_safe(entry, tmp, &freed_data_list, efd_list)
+		ext4_free_data_in_buddy(sb, entry);
 }
 
 int __init ext4_init_mballoc(void)
-- 
2.44.0.278.ge034bb2e1d-goog


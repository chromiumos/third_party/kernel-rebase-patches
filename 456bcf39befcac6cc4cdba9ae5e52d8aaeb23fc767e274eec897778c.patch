From bfa440af938db5a8d7083d99759eaa58d3e12d91 Mon Sep 17 00:00:00 2001
From: Guenter Roeck <groeck@chromium.org>
Date: Fri, 21 Sep 2018 09:33:24 -0700
Subject: [PATCH] CHROMIUM: alt-syscall: Make required syscalls available for
 use

Syscalls required by alt-syscall can not be called directly. Unless syscalls
are already made available for kernel use, create the necessary functions
as well as wrappers for the real syscalls.

BUG=chromium:885330
TEST=Build and run on any target or in qemu

Change-Id: Ia0846a1e2a8c168c75046e24fd170d8a952cb46d
Signed-off-by: Guenter Roeck <groeck@chromium.org>

Conflicts (compat syscall API changes):
	include/linux/syscalls.h
	kernel/sys.c
	kernel/time/posix-timers.c
	kernel/time/time.c

[rebase54(groeck): Handle conflicts]
Signed-off-by: Guenter Roeck <groeck@chromium.org>

[rebase510(groeck):
    Conflicts:
	include/linux/syscalls.h
	kernel/time/time.c
]
Signed-off-by: Guenter Roeck <groeck@chromium.org>

Change-Id: Ida4e7d79f8273f50dc5214bef903cdf8fe00a632
---
 include/linux/syscalls.h   | 21 +++++++++++++++++++++
 kernel/events/core.c       | 12 +++++++++---
 kernel/sys.c               | 34 ++++++++++++++++++++++++++++------
 kernel/time/posix-timers.c | 19 +++++++++++++++----
 kernel/time/time.c         | 15 +++++++++++++--
 5 files changed, 86 insertions(+), 15 deletions(-)

diff --git a/include/linux/syscalls.h b/include/linux/syscalls.h
--- a/include/linux/syscalls.h
+++ b/include/linux/syscalls.h
@@ -1381,4 +1381,25 @@ int __sys_getsockopt(int fd, int level, int optname, char __user *optval,
 		int __user *optlen);
 int __sys_setsockopt(int fd, int level, int optname, char __user *optval,
 		int optlen);
+
+#ifdef CONFIG_ALT_SYSCALL
+
+/* Only used with ALT_SYSCALL enabled */
+
+int ksys_prctl(int option, unsigned long arg2, unsigned long arg3,
+	       unsigned long arg4, unsigned long arg5);
+int ksys_setpriority(int which, int who, int niceval);
+int ksys_getpriority(int which, int who);
+int ksys_perf_event_open(
+		struct perf_event_attr __user *attr_uptr,
+		pid_t pid, int cpu, int group_fd, unsigned long flags);
+int ksys_clock_adjtime(const clockid_t which_clock, struct __kernel_timex __user * utx);
+int ksys_adjtimex(struct __kernel_timex __user *txc_p);
+int ksys_getcpu(unsigned __user *cpu, unsigned __user *node,
+		struct getcpu_cache __user *cache);
+int ksys_clock_adjtime32(clockid_t which_clock,
+			 struct old_timex32 __user *utp);
+int ksys_adjtimex_time32(struct old_timex32 __user *utp);
+#endif /* CONFIG_ALT_SYSCALL */
+
 #endif
diff --git a/kernel/events/core.c b/kernel/events/core.c
--- a/kernel/events/core.c
+++ b/kernel/events/core.c
@@ -11958,9 +11958,8 @@ perf_check_permission(struct perf_event_attr *attr, struct task_struct *task)
  * @group_fd:		group leader event fd
  * @flags:		perf event open flags
  */
-SYSCALL_DEFINE5(perf_event_open,
-		struct perf_event_attr __user *, attr_uptr,
-		pid_t, pid, int, cpu, int, group_fd, unsigned long, flags)
+int ksys_perf_event_open(struct perf_event_attr __user * attr_uptr, pid_t pid,
+			 int cpu, int group_fd, unsigned long flags)
 {
 	struct perf_event *group_leader = NULL, *output_event = NULL;
 	struct perf_event *event, *sibling;
@@ -12405,6 +12404,13 @@ SYSCALL_DEFINE5(perf_event_open,
 	return err;
 }
 
+SYSCALL_DEFINE5(perf_event_open,
+		struct perf_event_attr __user *, attr_uptr,
+		pid_t, pid, int, cpu, int, group_fd, unsigned long, flags)
+{
+	return ksys_perf_event_open(attr_uptr, pid, cpu, group_fd, flags);
+}
+
 /**
  * perf_event_create_kernel_counter
  *
diff --git a/kernel/sys.c b/kernel/sys.c
--- a/kernel/sys.c
+++ b/kernel/sys.c
@@ -200,7 +200,7 @@ static int set_one_prio(struct task_struct *p, int niceval, int error)
 	return error;
 }
 
-SYSCALL_DEFINE3(setpriority, int, which, int, who, int, niceval)
+int ksys_setpriority(int which, int who, int niceval)
 {
 	struct task_struct *g, *p;
 	struct user_struct *user;
@@ -264,13 +264,18 @@ SYSCALL_DEFINE3(setpriority, int, which, int, who, int, niceval)
 	return error;
 }
 
+SYSCALL_DEFINE3(setpriority, int, which, int, who, int, niceval)
+{
+	return ksys_setpriority(which, who, niceval);
+}
+
 /*
  * Ugh. To avoid negative return values, "getpriority()" will
  * not return the normal nice-value, but a negated value that
  * has been offset by 20 (ie it returns 40..1 instead of -20..19)
  * to stay compatible.
  */
-SYSCALL_DEFINE2(getpriority, int, which, int, who)
+int ksys_getpriority(int which, int who)
 {
 	struct task_struct *g, *p;
 	struct user_struct *user;
@@ -335,6 +340,11 @@ SYSCALL_DEFINE2(getpriority, int, which, int, who)
 	return retval;
 }
 
+SYSCALL_DEFINE2(getpriority, int, which, int, who)
+{
+	return ksys_getpriority(which, who);
+}
+
 /*
  * Unprivileged users may change the real gid to the effective gid
  * or vice versa.  (BSD-style)
@@ -2261,8 +2271,8 @@ int __weak arch_prctl_spec_ctrl_set(struct task_struct *t, unsigned long which,
 
 #define PR_IO_FLUSHER (PF_MEMALLOC_NOIO | PF_LOCAL_THROTTLE)
 
-SYSCALL_DEFINE5(prctl, int, option, unsigned long, arg2, unsigned long, arg3,
-		unsigned long, arg4, unsigned long, arg5)
+int ksys_prctl(int option, unsigned long arg2, unsigned long arg3,
+	       unsigned long arg4, unsigned long arg5)
 {
 	struct task_struct *me = current;
 	unsigned char comm[sizeof(me->comm)];
@@ -2537,8 +2547,14 @@ SYSCALL_DEFINE5(prctl, int, option, unsigned long, arg2, unsigned long, arg3,
 	return error;
 }
 
-SYSCALL_DEFINE3(getcpu, unsigned __user *, cpup, unsigned __user *, nodep,
-		struct getcpu_cache __user *, unused)
+SYSCALL_DEFINE5(prctl, int, option, unsigned long, arg2, unsigned long, arg3,
+		unsigned long, arg4, unsigned long, arg5)
+{
+	return ksys_prctl(option, arg2, arg3, arg4, arg5);
+}
+
+int ksys_getcpu(unsigned __user *cpup, unsigned __user *nodep,
+		struct getcpu_cache __user *unused)
 {
 	int err = 0;
 	int cpu = raw_smp_processor_id();
@@ -2550,6 +2566,12 @@ SYSCALL_DEFINE3(getcpu, unsigned __user *, cpup, unsigned __user *, nodep,
 	return err ? -EFAULT : 0;
 }
 
+SYSCALL_DEFINE3(getcpu, unsigned __user *, cpup, unsigned __user *, nodep,
+		struct getcpu_cache __user *, unused)
+{
+	return ksys_getcpu(cpup, nodep, unused);
+}
+
 /**
  * do_sysinfo - fill in sysinfo struct
  * @info: pointer to buffer to fill
diff --git a/kernel/time/posix-timers.c b/kernel/time/posix-timers.c
--- a/kernel/time/posix-timers.c
+++ b/kernel/time/posix-timers.c
@@ -1109,8 +1109,8 @@ int do_clock_adjtime(const clockid_t which_clock, struct __kernel_timex * ktx)
 	return kc->clock_adj(which_clock, ktx);
 }
 
-SYSCALL_DEFINE2(clock_adjtime, const clockid_t, which_clock,
-		struct __kernel_timex __user *, utx)
+int ksys_clock_adjtime(const clockid_t which_clock,
+		       struct __kernel_timex __user * utx)
 {
 	struct __kernel_timex ktx;
 	int err;
@@ -1126,6 +1126,12 @@ SYSCALL_DEFINE2(clock_adjtime, const clockid_t, which_clock,
 	return err;
 }
 
+SYSCALL_DEFINE2(clock_adjtime, const clockid_t, which_clock,
+		struct __kernel_timex __user *, utx)
+{
+	return ksys_clock_adjtime(which_clock, utx);
+}
+
 SYSCALL_DEFINE2(clock_getres, const clockid_t, which_clock,
 		struct __kernel_timespec __user *, tp)
 {
@@ -1179,8 +1185,7 @@ SYSCALL_DEFINE2(clock_gettime32, clockid_t, which_clock,
 	return err;
 }
 
-SYSCALL_DEFINE2(clock_adjtime32, clockid_t, which_clock,
-		struct old_timex32 __user *, utp)
+int ksys_clock_adjtime32(clockid_t which_clock, struct old_timex32 __user * utp)
 {
 	struct __kernel_timex ktx;
 	int err;
@@ -1197,6 +1202,12 @@ SYSCALL_DEFINE2(clock_adjtime32, clockid_t, which_clock,
 	return err;
 }
 
+SYSCALL_DEFINE2(clock_adjtime32, clockid_t, which_clock,
+		struct old_timex32 __user *, utp)
+{
+	return ksys_clock_adjtime32(which_clock, utp);
+}
+
 SYSCALL_DEFINE2(clock_getres_time32, clockid_t, which_clock,
 		struct old_timespec32 __user *, tp)
 {
diff --git a/kernel/time/time.c b/kernel/time/time.c
--- a/kernel/time/time.c
+++ b/kernel/time/time.c
@@ -266,7 +266,7 @@ COMPAT_SYSCALL_DEFINE2(settimeofday, struct old_timeval32 __user *, tv,
 #endif
 
 #ifdef CONFIG_64BIT
-SYSCALL_DEFINE1(adjtimex, struct __kernel_timex __user *, txc_p)
+int ksys_adjtimex(struct __kernel_timex __user * txc_p)
 {
 	struct __kernel_timex txc;		/* Local copy of parameter */
 	int ret;
@@ -280,6 +280,11 @@ SYSCALL_DEFINE1(adjtimex, struct __kernel_timex __user *, txc_p)
 	ret = do_adjtimex(&txc);
 	return copy_to_user(txc_p, &txc, sizeof(struct __kernel_timex)) ? -EFAULT : ret;
 }
+
+SYSCALL_DEFINE1(adjtimex, struct __kernel_timex __user *, txc_p)
+{
+	return ksys_adjtimex(txc_p);
+}
 #endif
 
 #ifdef CONFIG_COMPAT_32BIT_TIME
@@ -346,7 +351,7 @@ int put_old_timex32(struct old_timex32 __user *utp, const struct __kernel_timex
 	return 0;
 }
 
-SYSCALL_DEFINE1(adjtimex_time32, struct old_timex32 __user *, utp)
+int ksys_adjtimex_time32(struct old_timex32 __user * utp)
 {
 	struct __kernel_timex txc;
 	int err, ret;
@@ -363,6 +368,12 @@ SYSCALL_DEFINE1(adjtimex_time32, struct old_timex32 __user *, utp)
 
 	return ret;
 }
+
+SYSCALL_DEFINE1(adjtimex_time32, struct old_timex32 __user *, utp)
+{
+	return ksys_adjtimex_time32(utp);
+}
+
 #endif
 
 /*
-- 
2.33.0.464.g1972c5931b-goog


From a97f7ba15b352a054efe2b80907603991a3f9dac Mon Sep 17 00:00:00 2001
From: maciek swiech <drmasquatch@google.com>
Date: Mon, 9 Oct 2023 15:47:46 -0600
Subject: [PATCH] CHROMIUM: mm: allow for non-zram disk-based swap

current mm code only allows for disk-based swap using zram. this patch
will allow for normal block devices to be used as well. patch also uses
the DISK_BASED_SWAP kconfig and deprecates disk_based_swap sysctl.

BUG=b:215263397
BUG=b:303530401
TEST=emerge-tatl chromeos-kernel-6_1 && emerge-volteer termina-dlc
TEST=cros deploy termina-dlc
TEST=(host) truncate -s 2G swap.img &&
  vmc start termina --extra-disk swap.img
TEST=(termina) umount /mnt/external/0 &&
  mkswap /dev/vdc && swapon /dev/vdc
UPSTREAM-TASK=b:304355761

Change-Id: Id75185797dc1665095412c4af69bcc5a23b48355
Signed-off-by: maciek swiech <drmasquatch@google.com>
Reviewed-on: https://chromium-review.googlesource.com/c/chromiumos/third_party/kernel/+/4926929
Reviewed-by: Dmitry Torokhov <dtor@chromium.org>
---
 include/linux/mm.h |  2 --
 init/Kconfig       |  2 +-
 kernel/sysctl.c    | 11 -----------
 mm/swapfile.c      |  9 +++------
 4 files changed, 4 insertions(+), 20 deletions(-)

diff --git a/include/linux/mm.h b/include/linux/mm.h
index e99983b87f7f0a97f8ba9371d9adab9589dc4fbd..bf5d0b1b16f4341e8851d9bdfef30df631f62226 100644
--- a/include/linux/mm.h
+++ b/include/linux/mm.h
@@ -202,8 +202,6 @@ extern int sysctl_overcommit_memory;
 extern int sysctl_overcommit_ratio;
 extern unsigned long sysctl_overcommit_kbytes;
 
-extern int sysctl_disk_based_swap;
-
 int overcommit_ratio_handler(struct ctl_table *, int, void *, size_t *,
 		loff_t *);
 int overcommit_kbytes_handler(struct ctl_table *, int, void *, size_t *,
diff --git a/init/Kconfig b/init/Kconfig
index 8c590c8d8dce49f34bd6f22a0d8b1a0f42ec4496..35419f7520338861e91d6b1a7115262da0348065 100644
--- a/init/Kconfig
+++ b/init/Kconfig
@@ -374,7 +374,7 @@ config DISK_BASED_SWAP
 	default n
 	help
 	  By default, the Chromium OS kernel allows swapping only to
-	  zram devices. This option allows you to use disk-based files
+	  zram devices. This option allows you to use disk and disk-based files
 	  as swap devices too.  If unsure say N.
 
 config SYSVIPC
diff --git a/kernel/sysctl.c b/kernel/sysctl.c
index 269333d25e759c3d76f5d1583d4ad8a5a192603e..354a2d294f526ad6688168443913385eda101fa1 100644
--- a/kernel/sysctl.c
+++ b/kernel/sysctl.c
@@ -2248,17 +2248,6 @@ static struct ctl_table vm_table[] = {
 		.extra1		= (void *)&mmap_rnd_compat_bits_min,
 		.extra2		= (void *)&mmap_rnd_compat_bits_max,
 	},
-#endif
-#ifdef CONFIG_DISK_BASED_SWAP
-	{
-		.procname	= "disk_based_swap",
-		.data		= &sysctl_disk_based_swap,
-		.maxlen		= sizeof(sysctl_disk_based_swap),
-		.mode		= 0644,
-		.proc_handler	= proc_dointvec_minmax,
-		.extra1		= SYSCTL_ZERO,
-		.extra2		= SYSCTL_ONE,
-	},
 #endif
 	{ }
 };
diff --git a/mm/swapfile.c b/mm/swapfile.c
index 049d13a2f37e4ed4d277c06ce46d468ce57bfc68..ff07862bd48cfef0958b727c9162415669a4bf39 100644
--- a/mm/swapfile.c
+++ b/mm/swapfile.c
@@ -2770,14 +2770,11 @@ static struct swap_info_struct *alloc_swap_info(void)
 	return p;
 }
 
-/* This sysctl is only exposed when CONFIG_DISK_BASED_SWAP is enabled. */
-int sysctl_disk_based_swap;
-
 static int claim_swapfile(struct swap_info_struct *p, struct inode *inode)
 {
 	int error;
+	bool disk_based_swap_enabled = IS_ENABLED(CONFIG_DISK_BASED_SWAP);
 
-	/* On Chromium OS, we only support zram swap devices. */
 	if (S_ISBLK(inode->i_mode)) {
 		char name[BDEVNAME_SIZE];
 
@@ -2789,7 +2786,7 @@ static int claim_swapfile(struct swap_info_struct *p, struct inode *inode)
 			return error;
 		}
 		snprintf(name, sizeof(name), "%pg", p->bdev);
-		if (strncmp(name, "zram", strlen("zram"))) {
+		if (!disk_based_swap_enabled && strncmp(name, "zram", strlen("zram"))) {
 			iput(p->bdev->bd_inode);
 			p->bdev = NULL;
 			return -EINVAL;
@@ -2807,7 +2804,7 @@ static int claim_swapfile(struct swap_info_struct *p, struct inode *inode)
 			return -EINVAL;
 		p->flags |= SWP_BLKDEV;
 	} else if (S_ISREG(inode->i_mode)) {
-		if (!sysctl_disk_based_swap)
+		if (!disk_based_swap_enabled)
 			return -EINVAL;
 		p->bdev = inode->i_sb->s_bdev;
 	}
-- 
2.42.0.655.g421f12c284-goog


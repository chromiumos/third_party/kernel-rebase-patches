From f4ce44383f3725506f5ca40c9c670aa5408cb307 Mon Sep 17 00:00:00 2001
From: Devaraj Rangasamy <Devaraj.Rangasamy@amd.com>
Date: Wed, 30 Aug 2023 01:49:42 +0530
Subject: [PATCH] FROMLIST: tee: amdtee: use page_alloc_exact() for memory
 allocations

Use page_alloc_exact() to get buffers, instead of
get_free_pages(), so as to avoid wastage of memory.
Currently get_free_pages() is allocating at next order,
while page_alloc_exact() will free the unused pages.

Signed-off-by: Devaraj Rangasamy <Devaraj.Rangasamy@amd.com>
(am from https://lore.kernel.org/r/fe6fd7781d77ea32064e5b861f33150b28768278.1693340098.git.Devaraj.Rangasamy@amd.com)

BUG=b:285231416
UPSTREAM-TASK=b:308177821
TEST=CQ

Change-Id: I51f9b6d2862f0381af0e0c97c5c358a5544f8c2e
Signed-off-by: Tim Van Patten <timvp@google.com>
Reviewed-on: https://chromium-review.googlesource.com/c/chromiumos/third_party/kernel/+/4982592
Reviewed-by: Mark Hasemeyer <markhas@google.com>
---
 drivers/tee/amdtee/shm_pool.c | 18 ++++++++----------
 1 file changed, 8 insertions(+), 10 deletions(-)

diff --git a/drivers/tee/amdtee/shm_pool.c b/drivers/tee/amdtee/shm_pool.c
index 6346e0bc8a64828d1a66f4c659e2ae3dc18ea3ce..7251dedd2890cc9eb88cdb3a8936b46cb262576d 100644
--- a/drivers/tee/amdtee/shm_pool.c
+++ b/drivers/tee/amdtee/shm_pool.c
@@ -4,6 +4,7 @@
  */
 
 #include <linux/slab.h>
+#include <linux/mm.h>
 #include <linux/tee_core.h>
 #include <linux/psp.h>
 #include "amdtee_private.h"
@@ -11,26 +12,23 @@
 static int pool_op_alloc(struct tee_shm_pool *pool, struct tee_shm *shm,
 			 size_t size, size_t align)
 {
-	unsigned int order = get_order(size);
-	unsigned long va;
+	void *va;
 	int rc;
 
-	/*
-	 * Ignore alignment since this is already going to be page aligned
-	 * and there's no need for any larger alignment.
-	 */
-	va = __get_free_pages(GFP_KERNEL | __GFP_ZERO, order);
+	size = PAGE_ALIGN(size);
+
+	va = alloc_pages_exact(size, GFP_KERNEL | __GFP_ZERO);
 	if (!va)
 		return -ENOMEM;
 
 	shm->kaddr = (void *)va;
 	shm->paddr = __psp_pa((void *)va);
-	shm->size = PAGE_SIZE << order;
+	shm->size = size;
 
 	/* Map the allocated memory in to TEE */
 	rc = amdtee_map_shmem(shm);
 	if (rc) {
-		free_pages(va, order);
+		free_pages_exact(va, size);
 		shm->kaddr = NULL;
 		return rc;
 	}
@@ -42,7 +40,7 @@ static void pool_op_free(struct tee_shm_pool *pool, struct tee_shm *shm)
 {
 	/* Unmap the shared memory from TEE */
 	amdtee_unmap_shmem(shm);
-	free_pages((unsigned long)shm->kaddr, get_order(shm->size));
+	free_pages_exact(shm->kaddr, shm->size);
 	shm->kaddr = NULL;
 }
 
-- 
2.45.1.288.g0e0cd299f1-goog


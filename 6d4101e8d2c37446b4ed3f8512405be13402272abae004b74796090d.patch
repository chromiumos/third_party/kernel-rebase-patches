From 5704134eda6cfa33453f1cdcc8ae69f67c1ef298 Mon Sep 17 00:00:00 2001
From: Andrey Pronin <apronin@chromium.org>
Date: Wed, 20 Jul 2016 12:29:04 -0700
Subject: [PATCH] CHROMIUM: tpm: add legacy sysfs attributes for tpm2

Userland scripts and tests rely on certain sysfs attributes
present in the faimilar location:
/sys/class/tpm/tpm0/device/enabled
/sys/class/tpm/tpm0/device/owned

This change:
1) Adds two RO properties to sysfs for tpm in TPM2.0 case:
   - owned: set to 1 if storage hierarchy is enabled
     (TPMA_STARTUP_CLEAR.shEnable == 1)
   - enabled: set to 1 if an owner authorizaton is set
     (TPMA_PERMANENT.ownerAuthSet == 1)
2) Makes the sysfs attributes available at the legacy location:
   the attributes are now created under /sys/class/tpm/tpm0/,
   so symlinks are created in /sys/class/tpm/tpm0/device.

BUG=chrome-os-partner:55219
TEST=Boot on kevin, verify that enabled and owned properties
     are present.

Change-Id: I024d1fc34bd7c833dd0177b3aa06769c4c4f0bd1
Signed-off-by: Andrey Pronin <apronin@chromium.org>
Reviewed-on: https://chromium-review.googlesource.com/362383
Reviewed-by: Dmitry Torokhov <dtor@chromium.org>
---
 drivers/char/tpm/tpm-chip.c  | 6 +++---
 drivers/char/tpm/tpm-sysfs.c | 8 ++++++++
 2 files changed, 11 insertions(+), 3 deletions(-)

diff --git a/drivers/char/tpm/tpm-chip.c b/drivers/char/tpm/tpm-chip.c
index 783d65fc71f07da60dc777a0a3cd4b76f3ad2366..8e3ac70e6972154b3c517352845c08dfccec4e03 100644
--- a/drivers/char/tpm/tpm-chip.c
+++ b/drivers/char/tpm/tpm-chip.c
@@ -466,8 +466,8 @@ static void tpm_del_legacy_sysfs(struct tpm_chip *chip)
 {
 	struct attribute **i;
 
-	if (chip->flags & (TPM_CHIP_FLAG_TPM2 | TPM_CHIP_FLAG_VIRTUAL) ||
-	    tpm_is_firmware_upgrade(chip))
+	if (chip->flags & TPM_CHIP_FLAG_VIRTUAL ||
+		tpm_is_firmware_upgrade(chip))
 		return;
 
 	sysfs_remove_link(&chip->dev.parent->kobj, "ppi");
@@ -485,7 +485,7 @@ static int tpm_add_legacy_sysfs(struct tpm_chip *chip)
 	struct attribute **i;
 	int rc;
 
-	if (chip->flags & (TPM_CHIP_FLAG_TPM2 | TPM_CHIP_FLAG_VIRTUAL) ||
+	if (chip->flags & TPM_CHIP_FLAG_VIRTUAL ||
 		tpm_is_firmware_upgrade(chip))
 		return 0;
 
diff --git a/drivers/char/tpm/tpm-sysfs.c b/drivers/char/tpm/tpm-sysfs.c
index 19ddb4c2207c3fe88f73edee1b0e69c77a806cb9..0116f46d19777d6c50b47dba9ab6d9365eaf12b4 100644
--- a/drivers/char/tpm/tpm-sysfs.c
+++ b/drivers/char/tpm/tpm-sysfs.c
@@ -423,6 +423,12 @@ TPM2_PROP_FLAG_ATTR(ph_enable_nv,
 TPM2_PROP_FLAG_ATTR(orderly,
 		    TPM2_PT_STARTUP_CLEAR, TPM2_ATTR_ORDERLY);
 
+/* Aliases for userland scripts in TPM2 case */
+TPM2_PROP_FLAG_ATTR(enabled,
+		    TPM2_PT_STARTUP_CLEAR, TPM2_ATTR_SH_ENABLE);
+TPM2_PROP_FLAG_ATTR(owned,
+		    TPM2_PT_PERMANENT, TPM2_ATTR_OWNER_AUTH_SET);
+
 TPM2_PROP_U32_ATTR(lockout_counter, TPM2_PT_LOCKOUT_COUNTER);
 TPM2_PROP_U32_ATTR(max_auth_fail, TPM2_PT_MAX_AUTH_FAIL);
 TPM2_PROP_U32_ATTR(lockout_interval, TPM2_PT_LOCKOUT_INTERVAL);
@@ -441,6 +447,8 @@ static struct attribute *tpm2_dev_attrs[] = {
 	ATTR_FOR_TPM2_PROP(eh_enable),
 	ATTR_FOR_TPM2_PROP(ph_enable_nv),
 	ATTR_FOR_TPM2_PROP(orderly),
+	ATTR_FOR_TPM2_PROP(enabled),
+	ATTR_FOR_TPM2_PROP(owned),
 	ATTR_FOR_TPM2_PROP(lockout_counter),
 	ATTR_FOR_TPM2_PROP(max_auth_fail),
 	ATTR_FOR_TPM2_PROP(lockout_interval),
-- 
2.38.1.584.g0f3c55d4c2-goog


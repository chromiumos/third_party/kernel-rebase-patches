From b9843b5ef90603fbe6319911419b689029c7106d Mon Sep 17 00:00:00 2001
From: Scott James Remnant <scott@ubuntu.com>
Date: Tue, 27 Oct 2009 10:05:32 +0000
Subject: [PATCH] UBUNTU: SAUCE: trace: add trace events for open(), exec() and
 uselib()

BugLink: http://bugs.launchpad.net/bugs/462111

This patch uses TRACE_EVENT to add tracepoints for the open(),
exec() and uselib() syscalls so that ureadahead can cheaply trace
the boot sequence to determine what to read to speed up the next.

It's not upstream because it will need to be rebased onto the syscall
trace events whenever that gets merged, and is a stop-gap.

[benzh: 3.14 rebase. Squashed 2 warning fixes. Fixed a new incompatible
pointer type error for trace_open_exec() due to upstream change]

Change-Id: I3bb5c3f6c2b4b7cfbe7045e342092a4d652bcef2
Signed-off-by: Scott James Remnant <scott@ubuntu.com>
Acked-by: Stefan Bader <stefan.bader@canonical.com>
Acked-by: Andy Whitcroft <andy.whitcroft@canonical.com>
Signed-off-by: Stefan Bader <stefan.bader@canonical.com>
Signed-off-by: Ben Zhang <benzh@chromium.org>

Conflicts:
	fs/exec.c
[rebase44(filbranden): Moved the trace_open_exec call to the block where
 fsnotify_open was.]
Signed-off-by: Filipe Brandenburger <filbranden@chromium.org>

Conflicts:
	fs/open.c
	fs/exec.c

[rebase412(groeck): Resolved conflicts]
Signed-off-by: Guenter Roeck <groeck@chromium.org>
[rebase510(groeck): Context conflicts (open syscalls rearranged)]
Signed-off-by: Guenter Roeck <groeck@chromium.org>
Change-Id: I81ae7cf6bc8e435829666fdf21d13c7fd2c5eba5
---
 fs/exec.c                       |  6 +++-
 fs/open.c                       |  4 +++
 include/trace/events/fs_trace.h | 53 +++++++++++++++++++++++++++++++++
 3 files changed, 62 insertions(+), 1 deletion(-)
 create mode 100644 include/trace/events/fs_trace.h

diff --git a/fs/exec.c b/fs/exec.c
index a0b1f0337a6280fe7bf0ff653901cf5252dfbd25..219af95161db906d3fbe5d5c97a19fc1bdef6d72 100644
--- a/fs/exec.c
+++ b/fs/exec.c
@@ -69,6 +69,7 @@
 #include <asm/mmu_context.h>
 #include <asm/tlb.h>
 
+#include <trace/events/fs_trace.h>
 #include <trace/events/task.h>
 #include "internal.h"
 
@@ -935,9 +936,12 @@ static struct file *do_open_execat(int fd, struct filename *name, int flags)
 	if (err)
 		goto exit;
 
-	if (name->name[0] != '\0')
+	if (name->name[0] != '\0') {
 		fsnotify_open(file);
 
+		trace_open_exec(name->name);
+	}
+
 out:
 	return file;
 
diff --git a/fs/open.c b/fs/open.c
index a81319b6177f69a4b905688136c0231a6a3700dc..c5434914937e87e3aa16c3236823b5eb58abedcb 100644
--- a/fs/open.c
+++ b/fs/open.c
@@ -36,6 +36,9 @@
 
 #include "internal.h"
 
+#define CREATE_TRACE_POINTS
+#include <trace/events/fs_trace.h>
+
 int do_truncate(struct user_namespace *mnt_userns, struct dentry *dentry,
 		loff_t length, unsigned int time_attrs, struct file *filp)
 {
@@ -1314,6 +1317,7 @@ static long do_sys_openat2(int dfd, const char __user *filename,
 		} else {
 			fsnotify_open(f);
 			fd_install(fd, f);
+			trace_do_sys_open(tmp->name, how->flags, how->mode);
 		}
 	}
 	putname(tmp);
diff --git a/include/trace/events/fs_trace.h b/include/trace/events/fs_trace.h
new file mode 100644
index 0000000000000000000000000000000000000000..d450ea162e62a0568e5f35c61a6a46ab438824b2
--- /dev/null
+++ b/include/trace/events/fs_trace.h
@@ -0,0 +1,55 @@
+#undef TRACE_SYSTEM
+#define TRACE_SYSTEM fs
+#undef TRACE_INCLUDE_FILE
+#define TRACE_INCLUDE_FILE fs_trace
+
+#if !defined(_TRACE_FS_H) || defined(TRACE_HEADER_MULTI_READ)
+#define _TRACE_FS_H
+
+#include <linux/fs.h>
+#include <linux/tracepoint.h>
+
+TRACE_EVENT(do_sys_open,
+
+	TP_PROTO(const char *filename, int flags, int mode),
+
+	TP_ARGS(filename, flags, mode),
+
+	TP_STRUCT__entry(
+		__string(	filename, filename		)
+		__field(	int, flags			)
+		__field(	int, mode			)
+	),
+
+	TP_fast_assign(
+		__assign_str(filename, filename);
+		__entry->flags = flags;
+		__entry->mode = mode;
+	),
+
+	TP_printk("\"%s\" %x %o",
+		  __get_str(filename), __entry->flags, __entry->mode)
+);
+
+TRACE_EVENT(open_exec,
+
+	TP_PROTO(const char *filename),
+
+	TP_ARGS(filename),
+
+	TP_STRUCT__entry(
+		__string(	filename, filename		)
+	),
+
+	TP_fast_assign(
+		__assign_str(filename, filename);
+	),
+
+	TP_printk("\"%s\"",
+		  __get_str(filename))
+);
+
+#endif /* _TRACE_FS_H */
+
+/* This part must be outside protection */
+#include <trace/define_trace.h>
-- 
2.38.1.584.g0f3c55d4c2-goog


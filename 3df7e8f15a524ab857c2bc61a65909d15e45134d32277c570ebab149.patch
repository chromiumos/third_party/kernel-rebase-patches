From 78d5dff934b5f9a1a3f8d35b25601f7d67f2bd85 Mon Sep 17 00:00:00 2001
From: Evan Green <evgreen@chromium.org>
Date: Fri, 29 Oct 2021 14:47:09 -0700
Subject: [PATCH] CHROMIUM: config: Normalize config

This change is a no-op, patching up previous config changes that were
committed without being normalized.

The CONFIG_KFENCE changes moved from base.config to x86_64/common.config
and arm64/common.config because it depends on HAVE_ARCH_KFENCE, which is
not true of arm32. This was introduced by https://crrev.com/c/3222058

The QCOM_SLEEP_STATS just moved a bit because
https://crrev.com/c/3237165 did a find/replace of the old name but
didn't update the position due to realphabetizing.

GOV_TEO also moved slightly, and seemed to come in from some direct
commit dc5a342a708351833c129c1fbbc77a8554247ae6 ("CHROMIUM: config:
update configs to latest in 5.10").

BUG=b:202167074
TEST=./chromeos/scripts/kernelconfig olddefconfig

Signed-off-by: Evan Green <evgreen@chromium.org>
Change-Id: I16a99b34b6f1a2637d6a1168024c22c9b34ad02d
Reviewed-on: https://chromium-review.googlesource.com/c/chromiumos/third_party/kernel/+/3253658
Tested-by: Guenter Roeck <groeck@chromium.org>
Reviewed-by: Guenter Roeck <groeck@chromium.org>
Commit-Queue: Guenter Roeck <groeck@chromium.org>
---
 chromeos/config/arm64/chromiumos-arm64.flavour.config    | 2 +-
 chromeos/config/arm64/chromiumos-qualcomm.flavour.config | 2 +-
 chromeos/config/arm64/common.config                      | 4 ++++
 chromeos/config/base.config                              | 6 +-----
 chromeos/config/x86_64/common.config                     | 2 ++
 5 files changed, 9 insertions(+), 7 deletions(-)

diff --git a/chromeos/config/arm64/chromiumos-arm64.flavour.config b/chromeos/config/arm64/chromiumos-arm64.flavour.config
--- a/chromeos/config/arm64/chromiumos-arm64.flavour.config
+++ b/chromeos/config/arm64/chromiumos-arm64.flavour.config
@@ -165,10 +165,10 @@ CONFIG_QCOM_SMEM=y
 CONFIG_QCOM_SMP2P=y
 CONFIG_QCOM_SMSM=y
 CONFIG_QCOM_SOCINFO=y
-CONFIG_QCOM_STATS=m
 CONFIG_QCOM_SPMI_ADC5=m
 CONFIG_QCOM_SPMI_ADC_TM5=m
 CONFIG_QCOM_SPMI_TEMP_ALARM=m
+CONFIG_QCOM_STATS=m
 CONFIG_QCOM_SYSMON=y
 CONFIG_QCOM_TSENS=y
 CONFIG_QCOM_WDT=y
diff --git a/chromeos/config/arm64/chromiumos-qualcomm.flavour.config b/chromeos/config/arm64/chromiumos-qualcomm.flavour.config
--- a/chromeos/config/arm64/chromiumos-qualcomm.flavour.config
+++ b/chromeos/config/arm64/chromiumos-qualcomm.flavour.config
@@ -84,10 +84,10 @@ CONFIG_QCOM_RPMHPD=y
 CONFIG_QCOM_SMEM=y
 CONFIG_QCOM_SMP2P=y
 CONFIG_QCOM_SOCINFO=y
-CONFIG_QCOM_STATS=m
 CONFIG_QCOM_SPMI_ADC5=m
 CONFIG_QCOM_SPMI_ADC_TM5=m
 CONFIG_QCOM_SPMI_TEMP_ALARM=m
+CONFIG_QCOM_STATS=m
 CONFIG_QCOM_SYSMON=y
 CONFIG_QCOM_TSENS=y
 CONFIG_QCOM_WDT=y
diff --git a/chromeos/config/arm64/common.config b/chromeos/config/arm64/common.config
--- a/chromeos/config/arm64/common.config
+++ b/chromeos/config/arm64/common.config
@@ -63,6 +63,10 @@ CONFIG_IRQ_TIME_ACCOUNTING=y
 CONFIG_JUMP_LABEL=y
 # CONFIG_KEYBOARD_ATKBD is not set
 CONFIG_KEYBOARD_GPIO=y
+CONFIG_KFENCE=y
+CONFIG_KFENCE_SAMPLE_INTERVAL=500
+# CONFIG_KFENCE_STATIC_KEYS is not set
+CONFIG_LRU_GEN=y
 CONFIG_MAC80211_DEBUGFS=y
 CONFIG_MAC80211_LEDS=y
 CONFIG_MMC_SDHCI_PLTFM=y
diff --git a/chromeos/config/base.config b/chromeos/config/base.config
--- a/chromeos/config/base.config
+++ b/chromeos/config/base.config
@@ -55,8 +55,8 @@ CONFIG_CPUSETS=y
 CONFIG_CPU_FREQ_GOV_USERSPACE=y
 CONFIG_CPU_FREQ_STAT=y
 CONFIG_CPU_IDLE_GOV_LADDER=y
-CONFIG_CPU_IDLE_GOV_TEO=y
 CONFIG_CPU_IDLE_GOV_MENU=y
+CONFIG_CPU_IDLE_GOV_TEO=y
 CONFIG_CRC7=m
 CONFIG_CROS_EC=y
 CONFIG_CROS_EC_PD_UPDATE=y
@@ -472,7 +472,3 @@ CONFIG_XFRM_USER=y
 CONFIG_ZISOFS=y
 CONFIG_ZRAM=m
 CONFIG_ZSMALLOC=y
-CONFIG_KFENCE=y
-# CONFIG_KFENCE_STATIC_KEYS is not set
-CONFIG_KFENCE_SAMPLE_INTERVAL=500
-CONFIG_KFENCE_NUM_OBJECTS=255
diff --git a/chromeos/config/x86_64/common.config b/chromeos/config/x86_64/common.config
--- a/chromeos/config/x86_64/common.config
+++ b/chromeos/config/x86_64/common.config
@@ -73,6 +73,8 @@ CONFIG_INTEL_RAPL=y
 CONFIG_INTEL_VBTN=m
 CONFIG_IO_DELAY_0XED=y
 CONFIG_JME=m
+CONFIG_KFENCE=y
+CONFIG_KFENCE_SAMPLE_INTERVAL=500
 CONFIG_LEGACY_VSYSCALL_EMULATE=y
 CONFIG_LPC_ICH=y
 CONFIG_LPC_SCH=y
-- 
2.33.1.1089.g2158813163f-goog


From bb4e01916b69c1012a2aeeff0d4febb16267ff01 Mon Sep 17 00:00:00 2001
From: Pin-yen Lin <treapking@chromium.org>
Date: Fri, 19 Jul 2024 19:05:36 +0800
Subject: [PATCH] BACKPORT: FROMGIT: drm/bridge: it6505: Disable IRQ when
 powered off

When the bridge is powered off, disable the IRQ until the next power on
to workaround an interrupt storm on some badly-designed hardware.

Signed-off-by: Pin-yen Lin <treapking@chromium.org>
Reviewed-by: Robert Foss <rfoss@kernel.org>
Signed-off-by: Robert Foss <rfoss@kernel.org>
Link: https://patchwork.freedesktop.org/patch/msgid/20240719110623.1850021-1-treapking@chromium.org
(cherry picked from commit 10ba3753665f77314cb475e1a4ad0785f4770c4b
 git://anongit.freedesktop.org/drm/drm-misc drm-misc-next)

Conflicts:
   drivers/gpu/drm/bridge/ite-it6505.c

BUG=b:353885794
TEST=cat /proc/interrupts

Change-Id: Ic315b9215f110374e1493b9da34fdd8184448d25
Reviewed-on: https://chromium-review.googlesource.com/c/chromiumos/third_party/kernel/+/5743113
Tested-by: Pin-yen Lin <treapking@chromium.org>
Reviewed-by: Hsin-Te Yuan <yuanhsinte@chromium.org>
Auto-Submit: Pin-yen Lin <treapking@chromium.org>
Commit-Queue: Hsin-Te Yuan <yuanhsinte@chromium.org>
---
 drivers/gpu/drm/bridge/ite-it6505.c | 17 ++++++++++++-----
 1 file changed, 12 insertions(+), 5 deletions(-)

diff --git a/drivers/gpu/drm/bridge/ite-it6505.c b/drivers/gpu/drm/bridge/ite-it6505.c
index b114e1b433a8ae68ea634fc6f11f5e07b71bf7cf..dc9f1eeb17c0b34076d9f74ba948c6cb258c624f 100644
--- a/drivers/gpu/drm/bridge/ite-it6505.c
+++ b/drivers/gpu/drm/bridge/ite-it6505.c
@@ -470,6 +470,8 @@ struct it6505 {
 	bool enable_drv_hold;
 
 	const struct drm_edid *cached_edid;
+
+	int irq;
 };
 
 struct it6505_step_train_para {
@@ -2634,6 +2636,8 @@ static int it6505_poweron(struct it6505 *it6505)
 	it6505_init(it6505);
 	it6505_lane_off(it6505);
 
+	enable_irq(it6505->irq);
+
 	return 0;
 }
 
@@ -2650,6 +2654,8 @@ static int it6505_poweroff(struct it6505 *it6505)
 		return 0;
 	}
 
+	disable_irq_nosync(it6505->irq);
+
 	if (pdata->gpiod_reset)
 		gpiod_set_value_cansleep(pdata->gpiod_reset, 0);
 
@@ -3623,7 +3629,7 @@ static int it6505_i2c_probe(struct i2c_client *client)
 	struct it6505 *it6505;
 	struct device *dev = &client->dev;
 	struct extcon_dev *extcon;
-	int err, intp_irq, ret;
+	int err, ret;
 
 	it6505 = devm_kzalloc(&client->dev, sizeof(*it6505), GFP_KERNEL);
 	if (!it6505)
@@ -3675,17 +3681,18 @@ static int it6505_i2c_probe(struct i2c_client *client)
 
 	it6505_parse_dt(it6505);
 
-	intp_irq = client->irq;
+	it6505->irq = client->irq;
 
-	if (!intp_irq) {
+	if (!it6505->irq) {
 		dev_err(dev, "Failed to get INTP IRQ");
 		err = -ENODEV;
 		return err;
 	}
 
-	err = devm_request_threaded_irq(&client->dev, intp_irq, NULL,
+	err = devm_request_threaded_irq(&client->dev, it6505->irq, NULL,
 					it6505_int_threaded_handler,
-					IRQF_TRIGGER_LOW | IRQF_ONESHOT,
+					IRQF_TRIGGER_LOW | IRQF_ONESHOT |
+					IRQF_NO_AUTOEN,
 					"it6505-intp", it6505);
 	if (err) {
 		dev_err(dev, "Failed to request INTP threaded IRQ: %d", err);
-- 
2.46.0.662.g92d0881bb0-goog


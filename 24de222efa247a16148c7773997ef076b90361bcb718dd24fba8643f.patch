From 89f60c6cd50079052fab446639594fa387c24169 Mon Sep 17 00:00:00 2001
From: Hsin-Yi Wang <hsinyi@chromium.org>
Date: Tue, 20 Sep 2022 15:36:50 +0800
Subject: [PATCH] CHROMIUM: arm64: dts: mt8186: distinguish touchpad in SKU ID

touchpad_elan and touchpad_synatics need to be distinguished by
different sku ids to avoid component occupying resources and making
the device unusable.

To avoid creating too many dtb, currently these different SKUs with
second source touchpad shares the same dtb. But we're able to split them
in the future if necessary.

BUG=b:237634409, b:244494928
TEST=emerge-corsola sys-kernel/chromeos-kernel-5_15

Signed-off-by: Hsin-Yi Wang <hsinyi@chromium.org>
Change-Id: I78d81e9fb2a82d3454e76ddf0b48e7d8fdea9061
Reviewed-on: https://chromium-review.googlesource.com/c/chromiumos/third_party/kernel/+/3907425
Reviewed-by: Knox Chiou <knoxchiou@chromium.org>
Commit-Queue: Knox Chiou <knoxchiou@chromium.org>
---
 arch/arm64/boot/dts/mediatek/Makefile         |  1 +
 ...mt8186-corsola-magneton-rev2-sku393218.dts | 21 -------------------
 .../mt8186-corsola-steelix-sku131072.dts      |  6 +++---
 .../mt8186-corsola-steelix-sku131073.dts      |  6 +++---
 .../dts/mediatek/mt8186-corsola-steelix.dtsi  |  1 +
 5 files changed, 8 insertions(+), 27 deletions(-)
 delete mode 100644 arch/arm64/boot/dts/mediatek/mt8186-corsola-magneton-rev2-sku393218.dts

diff --git a/arch/arm64/boot/dts/mediatek/Makefile b/arch/arm64/boot/dts/mediatek/Makefile
index 9329abb5375fab79a8c6d7ccea473e72f66c54f1..cbd590b0840cebf8173a32a8397739d14d1d4358 100644
--- a/arch/arm64/boot/dts/mediatek/Makefile
+++ b/arch/arm64/boot/dts/mediatek/Makefile
@@ -65,6 +65,7 @@ dtb-$(CONFIG_ARCH_MEDIATEK) += mt8186-corsola-magneton-sku393216.dtb
 dtb-$(CONFIG_ARCH_MEDIATEK) += mt8186-corsola-magneton-sku393217.dtb
 dtb-$(CONFIG_ARCH_MEDIATEK) += mt8186-corsola-magneton-sku393218.dtb
 dtb-$(CONFIG_ARCH_MEDIATEK) += mt8186-corsola-rusty-sku196608.dtb
+dtb-$(CONFIG_ARCH_MEDIATEK) += mt8186-corsola-rusty-rev2-sku196608.dtb
 dtb-$(CONFIG_ARCH_MEDIATEK) += mt8186-corsola-steelix-sku131072.dtb
 dtb-$(CONFIG_ARCH_MEDIATEK) += mt8186-corsola-steelix-sku131073.dtb
 dtb-$(CONFIG_ARCH_MEDIATEK) += mt8186-corsola-tentacool-sku327681.dtb
diff --git a/arch/arm64/boot/dts/mediatek/mt8186-corsola-magneton-rev2-sku393218.dts b/arch/arm64/boot/dts/mediatek/mt8186-corsola-magneton-rev2-sku393218.dts
deleted file mode 100644
index 42b200e1b3e813e5de28aa0830d0f649e350001d..0000000000000000000000000000000000000000
--- a/arch/arm64/boot/dts/mediatek/mt8186-corsola-magneton-rev2-sku393218.dts
+++ /dev/null
@@ -1,21 +0,0 @@
-// SPDX-License-Identifier: (GPL-2.0 OR MIT)
-/*
- * Copyright 2022 Google LLC
- */
-
-/dts-v1/;
-#include "mt8186-corsola-steelix.dtsi"
-
-/ {
-	model = "Google Magneton sku393218 board";
-	compatible = "google,steelix-rev2-sku393218", "google,steelix-rev2", "google,steelix",
-		     "mediatek,mt8186";
-};
-
-&gpio_keys {
-	status = "disabled";
-};
-
-&touchscreen {
-	status = "disabled";
-};
diff --git a/arch/arm64/boot/dts/mediatek/mt8186-corsola-steelix-sku131072.dts b/arch/arm64/boot/dts/mediatek/mt8186-corsola-steelix-sku131072.dts
index eae17bca85854425f5a6e1464026716edff88e8c..a6d8b5ab0fbe0c240cbe5ee8314bbbdac8d68b05 100644
--- a/arch/arm64/boot/dts/mediatek/mt8186-corsola-steelix-sku131072.dts
+++ b/arch/arm64/boot/dts/mediatek/mt8186-corsola-steelix-sku131072.dts
@@ -7,9 +7,9 @@
 #include "mt8186-corsola-steelix.dtsi"
 
 / {
-	model = "Google Steelix board";
-	compatible = "google,steelix-sku131072", "google,steelix",
-		     "mediatek,mt8186";
+	model = "Google Steelix sku131072 board";
+	compatible = "google,steelix-rev2-sku131072", "google,steelix-rev2-sku131074",
+		     "google,steelix-rev2", "google,steelix", "mediatek,mt8186";
 	chassis-type = "convertible";
 };
 
diff --git a/arch/arm64/boot/dts/mediatek/mt8186-corsola-steelix-sku131073.dts b/arch/arm64/boot/dts/mediatek/mt8186-corsola-steelix-sku131073.dts
index a55375b95d0d4f10d45fc24670ee10ccdc3a2b83..c4ae7509774736a53a4b300aae7b2a8a81ba432a 100644
--- a/arch/arm64/boot/dts/mediatek/mt8186-corsola-steelix-sku131073.dts
+++ b/arch/arm64/boot/dts/mediatek/mt8186-corsola-steelix-sku131073.dts
@@ -7,9 +7,9 @@
 #include "mt8186-corsola-steelix.dtsi"
 
 / {
-	model = "Google Steelix board";
-	compatible = "google,steelix-sku131073", "google,steelix",
-		     "mediatek,mt8186";
+	model = "Google Steelix sku131073 board";
+	compatible = "google,steelix-rev2-sku131073", "google,steelix-rev2-sku131075",
+		     "google,steelix-rev2", "google,steelix", "mediatek,mt8186";
 	chassis-type = "convertible";
 };
 
diff --git a/arch/arm64/boot/dts/mediatek/mt8186-corsola-steelix.dtsi b/arch/arm64/boot/dts/mediatek/mt8186-corsola-steelix.dtsi
index e74e886a00cbef4ba4025fe7101ca68a77069524..baa11bd7fd5376d21270d9b24abae625cfd4028c 100644
--- a/arch/arm64/boot/dts/mediatek/mt8186-corsola-steelix.dtsi
+++ b/arch/arm64/boot/dts/mediatek/mt8186-corsola-steelix.dtsi
@@ -116,6 +116,7 @@ touchscreen: touchscreen@5d {
 
 &i2c2 {
 	i2c-scl-internal-delay-ns = <22000>;
+	pinctrl-names = "default";
 
 	/* second source component */
 	trackpad@2c {
-- 
2.44.0.478.gd926399ef9-goog


From e492ec81130b564a0550133140b3f93af8952d1d Mon Sep 17 00:00:00 2001
From: Sergey Senozhatsky <senozhatsky@chromium.org>
Date: Fri, 20 Aug 2021 13:13:20 +0900
Subject: [PATCH] CHROMIUM: media: uvcvideo: add a quirk for ROI fixup

One of the UVC modules that we have returns an error when we try
to `v4l2-ctl -d /dev/videoX -c region_of_interest_auto=1`. The problem
boils down to the fact that default (current) ROI configuration is
invalid - rectangle coordinates outside of GET_MIN. So when we set
auto-controls to 1 what UVC passes to the firmware is invalid ROI
rectangle and valid auto-controls.

Add a fixup function that will check if the device has UVC_QUIRK_ROI
quirk bit set and hence requires ROI configuration fixup.

BUG=b:196800810
TEST=loaded uvc on brya and observed no changes (quirk bit is not set yet)

Signed-off-by: Sergey Senozhatsky <senozhatsky@chromium.org>
Change-Id: I167b8fd71b340dd7cdc5fed8ae2ef38e7f13ba0b
Reviewed-on: https://chromium-review.googlesource.com/c/chromiumos/third_party/kernel/+/3096740
Reviewed-by: Ricardo Ribalda <ribalda@chromium.org>

[rebase515(groeck):
  Squashed:
    FIXUP: CHROMIUM: media: uvcvideo: add a quirk for ROI fixup
]

Signed-off-by: Guenter Roeck <groeck@google.com>
Change-Id: Ia2f222af05688301f10bfd3ef9395d2a379e1e6c
---
 drivers/media/usb/uvc/uvc_ctrl.c | 45 ++++++++++++++++++++++++++++++++
 drivers/media/usb/uvc/uvcvideo.h |  1 +
 2 files changed, 46 insertions(+)

diff --git a/drivers/media/usb/uvc/uvc_ctrl.c b/drivers/media/usb/uvc/uvc_ctrl.c
index 278fc6d3e3fbe4a12a3634271260e60f137e24d8..30b7119c755dd3bce2eaee01eea64e8e2338520c 100644
--- a/drivers/media/usb/uvc/uvc_ctrl.c
+++ b/drivers/media/usb/uvc/uvc_ctrl.c
@@ -2598,6 +2598,50 @@ static void uvc_ctrl_prune_entity(struct uvc_device *dev,
 	}
 }
 
+static int uvc_ctrl_init_roi(struct uvc_device *dev, struct uvc_control *ctrl)
+{
+	const u8 entity[16] = UVC_GUID_UVC_CAMERA;
+	struct uvc_roi *def;
+	int ret;
+
+	if (ctrl->info.selector != UVC_CT_REGION_OF_INTEREST_CONTROL ||
+	    !uvc_entity_match_guid(ctrl->entity, entity) ||
+	    !(dev->quirks & UVC_QUIRK_REINIT_ROI))
+		return 0;
+
+	if (WARN_ON(sizeof(struct uvc_roi) != ctrl->info.size))
+		return -EINVAL;
+
+	def = (struct uvc_roi *)uvc_ctrl_data(ctrl, UVC_CTRL_DATA_DEF);
+
+	ret = uvc_query_ctrl(dev, UVC_GET_DEF, ctrl->entity->id, dev->intfnum,
+			     UVC_CT_REGION_OF_INTEREST_CONTROL, def,
+			     sizeof(struct uvc_roi));
+	if (ret)
+		goto out;
+
+	/*
+	 * Some firmwares have wrong GET_CURRENT configuration. E.g. it's
+	 * below GET_MIN, or have rectangle coordinates mixed up. This
+	 * causes problems sometimes, because we are unable to set
+	 * auto-controls value without first setting ROI rectangle to
+	 * valid configuration.
+	 *
+	 * We expect that default configuration is always correct and
+	 * is within the GET_MIN / GET_MAX boundaries.
+	 *
+	 * Set current ROI configuration to GET_DEF, so that we will
+	 * always have properly configured ROI.
+	 */
+	ret = uvc_query_ctrl(dev, UVC_SET_CUR, 1, dev->intfnum,
+			     UVC_CT_REGION_OF_INTEREST_CONTROL, def,
+			     sizeof(struct uvc_roi));
+out:
+	if (ret)
+		dev_err(&dev->udev->dev, "Failed to fixup ROI (%d).\n",  ret);
+	return ret;
+}
+
 /*
  * Add control information and hardcoded stock control mappings to the given
  * device.
@@ -2630,6 +2674,7 @@ static void uvc_ctrl_init_ctrl(struct uvc_video_chain *chain,
 			 * GET_INFO on standard controls.
 			 */
 			uvc_ctrl_get_flags(chain->dev, ctrl, &ctrl->info);
+			uvc_ctrl_init_roi(chain->dev, ctrl);
 			break;
 		 }
 	}
diff --git a/drivers/media/usb/uvc/uvcvideo.h b/drivers/media/usb/uvc/uvcvideo.h
index cb1d512bf3ac475f388d889b9498792f11c36f60..37a4f6dc8d16c915a78554877e198803eef6a13a 100644
--- a/drivers/media/usb/uvc/uvcvideo.h
+++ b/drivers/media/usb/uvc/uvcvideo.h
@@ -74,6 +74,7 @@
 #define UVC_QUIRK_FORCE_BPP		0x00001000
 #define UVC_QUIRK_WAKE_AUTOSUSPEND	0x00002000
 #define UVC_QUIRK_PRIVACY_DURING_STREAM	0x00004000
+#define UVC_QUIRK_REINIT_ROI		0x80000000
 
 /* Format flags */
 #define UVC_FMT_FLAG_COMPRESSED		0x00000001
-- 
2.34.1

